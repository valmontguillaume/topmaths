import type { StringGrade } from './grade'

export type ButtonColor = 'topmaths' | 'coopmaths' | 'sponsor' | 'fuchsia' | 'green' | 'link' | 'info' | 'warning' | 'danger' | 'purple' | 'info-darker'

export type ThemeColor = StringGrade | ButtonColor
