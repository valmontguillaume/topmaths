
#show: doc => page(flipped: true, columns: 2, margin: 1cm, doc)
#let exercice = {
  table(
    columns: (3fr, 1fr, 4fr, 1fr, 4fr),
    align: horizon + center,
    stroke: none,
    [Deux points\ distincts], [•], [], [•], image("../../cours/objectifs/6e/6G10-2.png", width: 10pt),
    [Deux points\ confondus], [•], [], [•], image("../../cours/objectifs/6e/6G10-11.png", width: 70pt),
    [Une droite], [•], [], [•], image("../../cours/objectifs/6e/6G10-13.png", width: 70pt),
    [Une\ demi-droite], [•], [], [•], image("../../cours/objectifs/6e/6G10-6.png", width: 70pt),
    [Un segment], [•], [], [•], image("../../cours/objectifs/6e/6G10-4.png", width: 70pt),
    [Milieu\ d'un segment], [•], [], [•], image("../../cours/objectifs/6e/6G10-1.png", width: 70pt),
    [Points alignés], [•], [], [•], image("../../cours/objectifs/6e/6G10-5.png", width: 70pt),
    [Droites sécantes], [•], [], [•], image("../../cours/objectifs/6e/6G10-10.png", width: 70pt),
    [Droites\ perpendiculaires], [•], [], [•], image("../../cours/objectifs/6e/6G10-9.png", width: 70pt),
    [Droites\ parallèles], [•], [], [•], image("../../cours/objectifs/6e/6G10-7.png", width: 70pt),
    [Polygone], [•], [], [•], image("../../cours/objectifs/6e/6G10-12.png", width: 70pt),
    [Diagonale], [•], [], [•], image("../../cours/objectifs/6e/6G10-8.png", width: 70pt),
  )
}
#exercice
#colbreak()
#exercice
