#import "../../preambule_photocopies.typ": * 
#show: doc => photocopies(doc, paysage: false, nbCols: 1)

#image("5P11-1.png")
#place(right, dy: -3.5em)[Échelle : $1/(30 000)$]
#place(right, dy:-2em, text(size: 0.7em,link("https://www.openstreetmap.org/copyright")[© OpenStreetMap contributors]))

#image("5P11-2.png")
#place(right, dy: -1em)[Échelle : $1/(312 500)$]
#place(right, dy:0.5em, text(size: 0.7em,link("https://www.openstreetmap.org/copyright")[© OpenStreetMap contributors]))
