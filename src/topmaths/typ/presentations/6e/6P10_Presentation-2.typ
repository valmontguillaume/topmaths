#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc)
#show: doc => normal(doc)

#slide()[
  #remarque()[
    On peut dire que deux grandeurs sont proportionnelles si elles se "multiplient de la même façon".

    Par exemple, si le nombre de bonbons est multiplié par 10, le prix est aussi multiplié par 10, donc les deux grandeurs sont proportionnelles.
  ]
]
