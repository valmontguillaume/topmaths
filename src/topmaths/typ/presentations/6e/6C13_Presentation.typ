#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "6C13 : Multiplier ou diviser par 0,1 ; 0,01 ; 0,001")
#show: doc => normal(doc)

#slide()[
  #proprietes()[
    Multiplier par 0,1 revient à diviser par 10.\
    Multiplier par 0,01 revient à diviser par 100.\
    etc.

    Diviser par 0,1 revient à multiplier par 10.\
    Diviser par 0,01 revient à multiplier par 100.\
    etc.
  ]
]
