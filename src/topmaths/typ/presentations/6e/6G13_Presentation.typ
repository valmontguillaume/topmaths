#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "6G13 : Reconnaître l’appartenance à une droite")
#show: doc => normal(doc)

#slide()[
  #definitions()[
    Des points #motDefini()[alignés] sont des points qui sont situés sur la même droite.\
    On dit alors qu'ils #motDefini()[appartiennent] à cette droite.
  ]

  #uncover(2)[
    #regle(titre: "Notations")[
      $in$ signifie « appartient à ».\
      $in.not$ signifie « n'appartient pas à ».
    ]
  ]
]
