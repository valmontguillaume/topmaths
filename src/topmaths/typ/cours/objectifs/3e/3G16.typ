#propriete()[
  Si deux triangles sont semblables alors l'un est un agrandissement de l'autre et les longueurs de leurs côtés sont proportionnelles.
]

#exemple()[
  Les marques identiques indique des angles égaux.\
  Calculer les longueurs des segments $[V T]$ et $[U T]$. Justifier.

  #set text(couleurPrincipale)
  #image("3G16-1.png")
  
  $hat(O N M) = hat(V U T)$\
  $hat(M O N) = hat(T V U)$\
  $hat(N M O) = hat(U T V)$\
  Les $3$ paires d'angles sont égales. Comme les angles sont égaux deux à deux, les deux triangles sont semblables.\
  Les longueurs $V U$, $V T$ et $U T$ sont donc proportionnelles à $O N$, $O M$ et $N M$ respectivement.\
  Le coefficient d'agrandissement est égal à $23,4 div 6,5 = 3,6$.\
  donc $V T = 3,6 times O M = 3,6 times 5 = 18$ cm\
  et $U T = 3,6 times N M = 3,6 times 7,5 = 27$ cm.
]
