#definition()[
  #motDefini()[L'étendue] d'une série statistique est la différence entre la plus grande valeur et la plus petite valeur d'une série.
]

#exemple(titre: "Exemple 1")[
  Voici les vitesses des vents en km/h relevés dans différentes villes lors du passage d'un cyclone : $142$ ; $174$ ; $ 112$ ; $153$ ; $205$\
  Quelle est l'étendue de ces vitesses ?

  #set text(couleurPrincipale)
  La plus grande valeur est $205$.\
  La plus petite valeur est $112$.\
  L'étendue est donc $205 - 112 = 103$.\
  Interprétation : Cela signifie que dans la ville où le vent a soufflé le plus fort, il a soufflé à $103$ km/h de plus que dans la ville où le vent à soufflé le moins fort.
]

#exemple(titre: "Exemple 2")[
  Voici les notes obtenues au mathématiques par deux élèves d'une même classe au cours du 1er trimestre.
  - Abdel : $5$ ; $9$ ; $10$ ; $12$ ; $15$ ; $15$
  - Noémie :  $9$ ; $9$ ; $11$ ; $11$ ; $13$ ; $13$
  On peut vérifier que les notes de Abdel et de Noémie ont la même moyenne et la même médiane : $11$.
  L'étendue ($10 – 5 = 10$ pour les notes de Abdel et $13 – 9 = 4$ pour les notes de Noémie) indique que les notes de Abdel sont plus dispersées que celles de Noémie (Noémie est plus régulière que Abdel)
]

#remarques()[
- On dit que l'étendue est un indicateur de dispersion.
- L'étendue est très sensible aux valeurs extrêmes d'une série.
]