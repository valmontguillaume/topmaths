#definitions()[
  #place(image("3S30-1.png", height: 4em), right, dy: 1em)
  Une #motDefini()[fonction] $f$ est un processus qui, à chaque valeur d’un nombre $x$, appelé #motDefini()[variable], associe un unique nombre $f(x)$.\
  Le nombre $f(x)$ est #motDefini()[l’image] de $x$ par la fonction $f$.\
  Le nombre $x$ est un #motDefini()[antécédent] de $f(x)$.\
  On note $f : x ⟼  f(x)$ (se lit "$f$ est la fonction qui, à $x$ associe $f(x)$")
]

#remarque()[
  On dit "l'image" car un nombre ne peut avoir qu'une seule image mais on dit "un antécédent" car un même nombre peut avoir plusieurs antécédents.
]

#exemple()[
  Une fonction $f$ associe à $x$ son carré.\
  On note $f : x -> x^2$ (qui se lit $f$, la fonction qui, à $x$ associe $x^2$);\
  ou encore $f(x) = x^2$ (qui se lit l’image de $x$ par la fonction f est $x^2$).

  $f(5) = 25$. L’image de $5$ par la fonction $f$ est $25$.\
  $f(–5) = 25$. L’image de $–5$ par la fonction $f$ est $25$.\
  $25$ a donc deux antécédents par la fonction $f$ qui sont $5$ et $–5$.
]
