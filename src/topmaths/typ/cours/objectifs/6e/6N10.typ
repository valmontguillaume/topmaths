#vocabulaire()[
  Pour écrire un nombre entier, on dispose de dix chiffres : 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9.#linebreak()
  C'est pour cela que notre système de numération est appelé système décimal (déci veut dire 10).
]
#exemple()[
  Les nombres 2020 et 2002 sont écrits avec les mêmes chiffres (0 et 2) mais dans un ordre différent.
]
#remarque()[
  #attention() Attention à ne pas confondre chiffre et nombre !]#linebreak()
  Ce sont les nombres qu’on additionne, qu’on soustrait, qu’on multiplie, qu’on divise, qu’on range etc. #rouge()[Les chiffres ne sont que des symboles] qui servent à les écrire !#linebreak()
  Par exemple on peut écrire le même calcul avec des chiffres arabes, des chiffres romains ou encore des sinogrammes (symboles chinois) :#linebreak()
  $1 + 2 = 3$#linebreak()
  #text(font: "STIX Two Text")[I + II = III]#linebreak()
  一 + 二 = 三#linebreak()
  Ce sont les mêmes calculs écrits avec les mêmes nombres mais avec des chiffres différents. Ils se lisent de la même façon : un plus deux égale trois.
]
#regle()[
  En numération arabe (celle qu’on utilise), la valeur d'un chiffre dépend de son rang (de sa position).
]
#exemple()[
  Dans le nombre #rouge()[4] 2#bleu()[4]3 76#vert()[4] le chiffre 4 a plusieurs valeurs différentes :
  - #rouge()[4] est le chiffre des millions et vaut donc 4 000 000 d'unités
  - #bleu()[4] est le chiffre des dizaines de milliers et vaut donc 40 000 unités.
  - #vert()[4] est le chiffre des unités et vaut donc 4 unités.
]
#exemple(titre: "Tableau de numération")[
  #image("6N10-1.png")
]
#regles()[
  - On place un trait d’union entre tous les mots qui composent le nombre.
  - Vingt et cent s'accordent au pluriel lorsqu'ils ne sont pas suivis d'un autre nombre.
  - Mille est invariable (il ne s’accorde jamais).
  - Million et milliard s'accordent au pluriel.
]
#exemples()[
  - 80 s’écrit quatre-ving#rouge()[ts] mais 83 s’écrit quatre-ving#rouge()[t]-trois.
  - 200 s’écrit deux-cen#rouge()[ts] mais 201 s’écrit deux-cen#rouge()[t]-un.
  - 3000 s’écrit trois-mill#rouge()[e] et 3900 s’écrit trois-mill#rouge()[e]-neuf-cents.
  - 1 000 000 s’écrit un million, 2 000 000 s’écrit deux-million#rouge()[s] et 3 000 100 s’écrit trois-million#rouge()[s]-cent.
  - 3 000 000 000 s’écrit Trois-milliard#rouge()[s]	3 400 000 000 s’écrit Trois-milliard#rouge()[s]-quatre-cent-millions.
]