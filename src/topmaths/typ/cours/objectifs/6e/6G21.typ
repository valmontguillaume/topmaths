#definitions()[
  Un #motDefini()[polygone] est une figure fermée uniquement composée de segments.\
  Les segments qui le composent sont appelés les #motDefini()[côtés] du polygone.\
  Les extrémités de ses côtés sont appelées les #motDefini()[sommets] du polygone.\
  Les segments qui relient deux sommets non consécutifs (qui ne se suivent pas) du polygone sont appelés des #motDefini()[diagonales] du polygone.
]

#regle()[
  Pour nommer un polygone, on part de n'importe quel sommet et on tourne #vert()[dans un sens ou dans l'autre] en notant les sommets #rouge()[dans l'ordre où on les rencontre].
]

#exemple()[
  #image("6G21-1.png", height: 8em)
  On peut nommet ce polygone DINOR mais aussi le nommer ORDIN, DRONI, IDRON…
]