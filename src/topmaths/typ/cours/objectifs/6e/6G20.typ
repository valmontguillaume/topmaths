#definition()[
  Un #motDefini()[cercle] de centre O est formé de tous les points situés à une même distance du point O.\
  Cette distance commune est appelée #motDefini()[le rayon] du cercle.
]

#exemple()[
  #table(
    columns: (35%, 5%, 60%),
    stroke: none,
    align: horizon,
    image("6G20-1.png"), [], [
      Ce cercle a pour centre le point O et pour rayon le nombre OM\
      Le segment [OM] est un rayon du cercle, tout comme [OC]\
      La longueur OM = OC = OD est le rayon du cercle\
      Le segment [DC] est un diamètre du cercle\
      La longueur DC est le diamètre du cercle\
      Le segment [AB] est une corde du cercle
  ])
]

#definition()[
  #motDefini()[Un rayon] du cercle est un segment ayant pour extrémités le centre du cercle et un point du cercle.
]

#remarque()[
  Le segment [OM] est #motDefini()[un] rayon du cercle. La longueur OM est #motDefini()[le] rayon du cercle.\
  #motDefini()[Le] rayon d'un cercle est un nombre tandis qu'#motDefini()[un] rayon du cercle est un segment.
]

#definitions()[
  Une #motDefini()[corde] est un segment dont les extrémités sont deux points du cercle.\
  #motDefini()[Un diamètre] est une corde passant par le centre du cercle.\
  #motDefini()[Le diamètre] d'un cercle est la longueur commune de tous les diamètres.
]

#propriete()[
  Le diamètre d'un cercle est égal au double de son rayon.
]