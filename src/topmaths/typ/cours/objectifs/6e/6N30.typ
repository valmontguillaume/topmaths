#exemple(titre: "Exemple 1")[
  #box(image("6N30-1.png", width: 43pt))
  #box(image("6N30-1.png", width: 43pt))
  #box(image("6N30-1.png", width: 43pt))
  #box(image("6N30-2.png", width: 43pt))\
  $4/4 = 1$ unité donc $13/4 = 3 times 4/4 + 1/4 = 3 + 1/4$
]

#exemple(titre: "Exemple 2")[
  #box(image("6N30-3.png", width: 60pt)) #h(10pt)
  #box(image("6N30-3.png", width: 60pt)) #h(10pt)
  #box(image("6N30-4.png", width: 60pt))\
  #v(0pt)
  $5/5 = 1$ unité donc $14/5 = 2 times 5/5 + 4/5 = 2 + 4/5$
]

#proprietes()[
  Une fraction dont le numérateur est égal au dénominateur vaut 1 unité.\
  Une fraction dont le numérateur est un multiple du dénominateur est un nombre entier.
]