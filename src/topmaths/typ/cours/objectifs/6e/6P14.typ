#definitions()[
  Lorsqu’on multiplie toutes les longueurs d’une figure ou d’un solide par un nombre #vert()[$k > 1$], on dit qu’on en fait un #motDefini()[agrandissement de rapport $k$].\
  Lorsqu’on multiplie toutes les longueurs d’une figure ou d’un solide par un nombre #vert()[$0 < k < 1$], on dit qu'on fait une #motDefini()[réduction de rapport $k$].
]

#exemples()[
  #image("6P14-1.png")
]

#remarque()[
  Si on multiplie toutes les longueurs d'une figure par #vert()[$k = 1$], les longueurs ne changent pas ! (Si on multiplie n'importe quel nombre par $1$, on obtient le même nombre) Ce n'est donc ni un agrandissement, ni une réduction.
]