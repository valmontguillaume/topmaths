#methode()[
  Pour multiplier un nombre décimal par 10, 100, 1 000... on compte le nombre de zéros et on décale la virgule vers la droite du même nombre de rangs en rajoutant des zéros si besoin.\
  Pour diviser un nombre décimal par 10, 100, 1 000... on fait la même chose mais vers la gauche !
]

#exemples()[
  - 9,487 $div$ 1#rouge()[0] = 9#rouge()[4,]87
  - 74,5 $times$ 1#rouge()[00] = 74#gris()[,]#rouge()[5\_,] $times$ 1#rouge()[00] = 74#rouge()[50]
  - 84 $div$ 1 #rouge()[000] = #rouge()[,\_84]#gris()[,] $div$ 1 #rouge()[000] = #vert()[0]#rouge()[,084]
]