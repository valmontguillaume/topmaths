#definition()[
  La #motDefini()[distance d'un point à une droite] est la plus courte distance séparant ce point de la droite.
]

#methode()[
  Pour trouver la distance entre un point $A$ et une droite $(d)$, on trace la droite perpendiculaire à $(d)$ passant par $A$.
  
  Si on appelle $B$ le point d’intersection entre les deux droites, alors la distance entre le point $A$ et la droite $(d)$ est la longueur du segment $[A B]$.
]

#exemple()[
  Détermine la distance du point $A$ à la droite $(d)$
  #show: normal
  #align(horizon, grid(columns: (3fr, 1fr, 3fr), image("6G14-1.png", height: 9em), [$->$], image("6G14-2.png", height: 9em)))
  La distance du point $A$ à la droite $(d)$ est la mesure du segment $[A B]$.\
  C'est-à-dire …… cm.
]
