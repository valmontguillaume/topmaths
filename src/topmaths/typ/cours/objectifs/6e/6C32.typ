#regle()[
  Pour faire un calcul lorsqu'il y a des additions, des soustractions et des multipliations, on fait #rouge()[d'abord les multiplications].\
  On dit que les multiplications sont #rouge()[prioritaires].
]

#exemples()[
  $3 + 8 times 2 = 19$\
  $3 times 5 + 2 = 17$\
  $2 times 3 + 4 times 5 = 26$
]