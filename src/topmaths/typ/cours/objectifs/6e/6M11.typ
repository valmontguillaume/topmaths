#methodes()[
  Pour calculer l'aire d'une figure complexe, il y a deux possibilités :
  + on la découpe en figures dont on sait calculer les aires et on les additionne ;
    #image("6M11-1.png", height: 4em)
  + on la découpe en figures dont on sait calculer les aires et on les soustrait.
    #image("6M11-2.png", height: 4em)
]