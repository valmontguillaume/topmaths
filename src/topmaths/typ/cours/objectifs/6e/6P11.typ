#exemple()[
  Nawel lit sur sa recette de crêpes pour 3 personnes qu'il faut 150 g de farine.\
  Elle veut adapter sa recette pour 21 personnes.\
  Quelle masse de farine doit-elle prévoir ?
]

On peut représenter ce problème par un schéma :
#image("6P11-1.png")
On voit sur le schéma qu’il y a deux moyens de résoudre ce problème :
- Le #rouge()[chemin en rouge] utilise ce qu’on appelle le #rouge()[coefficient de proportionnalité] qui permet de passer d’une grandeur (nombre de personnes) à l’autre (masse de farine).
- Le #vert()[chemin en vert] utilise le fait que si deux grandeurs sont proportionnelles, lorsque la première est multipliée par un nombre, la deuxième est aussi #vert()[multipliée par le même nombre].
Les deux chemins permettent d’arriver au même résultat mais selon les données de l’exercice, un chemin peut être beaucoup plus difficile que l’autre !

#grid(columns: 2, [Par exemple : #align(center, image("6P11-2.png", width: 90%))], [ou encore : #align(center, image("6P11-3.png", width: 90%))])

#remarques()[
  Ça marche aussi avec les divisions, et ça marche aussi dans l’autre sens !\
  Il faut juste faire attention à mettre les mêmes grandeurs dans les mêmes « colonnes » (le beurre sous le beurre par exemple)
  #image("6P11-4.png", width: 60%)
]