#definition()[
  Une #motDefini()[grandeur] est quelque chose qu'on peut #vert()[compter] ou qu'on peut #vert()[mesurer] #noir()[(par exemple une durée, une masse, un volume, une distance, un nombre de stylos, ...)].\
  Une #motDefini()[grandeur] est liée à une #vert()[unité] #noir()[(par exemple min, kg, litres, km, stylos, ...)].
]
#exemple()[
  Karole achète $3$ ananas à $2$ €.

  #set text(couleurPrincipale)
  Les grandeurs sont :
  - le nombre d'ananas (l'unité est "ananas")
  - le prix (l'unité est €)
]

#definitions()[
  Deux grandeurs sont #motDefini()[proportionnelles] si les valeurs de l’une s’obtiennent en multipliant les valeurs de l’autre par un même nombre appelé #motDefini()[coefficient de proportionnalité].
]

#exemple(titre: "Exemple 1")[
  Des letchis sont vendus à 3€ le kg. Le prix des letchis est-il proportionnel à leur masse ?\
  #normal()[
    On peut passer de la masse de letchis en kg à leur prix en € en multipliant par 3.\
    Le prix des letchis est donc proportionnel à leur masse et le coefficient de proportionnalité est 3.
  ]
]

#exemple(titre: "Exemple 2")[
  2 sachets de bonbons coûtent 5€ tandis que 3 sachets coûtent 6€.\
  Le prix est-il proportionnel au nombre de sachets ?\
  #normal()[
    5 ÷ 2 = 2,5\
    6 ÷ 3 = 2\
    On n’obtient pas le même résultat, le prix n’est donc pas proportionnel au nombre de sachets.
  ]
]

#remarque()[
  On peut dire que deux grandeurs sont proportionnelles si elles se "multiplient de la même façon".

  Par exemple, si le nombre de bonbons est multiplié par 10, le prix est aussi multiplié par 10, donc les deux grandeurs sont proportionnelles.
]