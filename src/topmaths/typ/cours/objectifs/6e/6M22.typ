#remarque()[
  Comme un volume s’exprime en #rouge()[$#normal("cm")^3$], #rouge()[$#normal("dm")^3$], #rouge()[$#normal(" m ")^3$], etc. il y a #rouge()[$3$] colonnes pour chaque unité dans le tableau de conversion.
]

#exemple()[
  #table(
    columns: (1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr),
    inset: 0.6em,
    align: center + horizon,
    table.cell(colspan: 3)[$"km"^3$], table.cell(colspan: 3)[$"hm"^3$], table.cell(colspan: 3)[$"dam"^3$], table.cell(colspan: 3)[$" m "^3$], table.cell(colspan: 3, $"dm"^3$), table.cell(colspan: 3, $"cm"^3$), table.cell(colspan: 3, $"mm"^3$),
    [\ ], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [],
    [\ ], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [],
    [\ ], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []
  )
]
