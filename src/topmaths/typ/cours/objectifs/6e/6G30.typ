#remarque()[
  Lorsqu'on rencontre une figure qui a une forme inhabituelle, on peut souvent la décomposer en figures que l'on connaît en les ajoutant on en les enlevant.
]

#exemple(titre: "Exemple 1")[
  #image("6G30-1.png", height: 5em)
  On remarque qu'on peut décomposer cette figure en un rectangle, un triangle-rectangle et un demi-disque.
  #image("6G30-2.png", height: 5em)
]

#exemple(titre: "Exemple 2")[
  #image("6G30-3.png", height: 8em)
  On remarque qu'on peut obtenir cette figure en prenant un carré complet de côté 8,5 cm et en enlevant un triangle rectangle dont les côtés de l'angle droit mesurent respectivement 3,6 cm et 7,7 cm.
  #image("6G30-4.png", height: 8em)
]