#methode()[
  Pour placer un nombre sur une demi-droite graduée, il faut repérer 3 choses :
  + un point de départ
  + à quoi correspondent les grandes graduations
  + à quoi correspondent les petites graduations

  Ensuite, à partir du point de départ on ajoute ou on enlève les valeurs de chaque grande et de chaque petite graduation jusqu'à atteindre le point qui nous intéresse.
]

#exemple(titre: "Exemple 1")[
  Placer le nombre 7,77 sur la demi-droite graduée ci-dessous.
  #image("6N20-1.png")
  #set text(couleurPrincipale)
  + Comme point de départ, on peut choisir 7,4 ou 7,6. Puisqu'on veut placer 7,7 on va choisir 7,6 qui est plus proche
  + Je remarque qu'il y a une seule grande graduation entre 7,4 et 7,6 ; c'est certainement 7,5. Ce qui signifie que chaque grande graduation vaut un dixième.
  + Je remarque que l'espace entre chaque grande graduation est partagé en 10 petits espaces donc chaque petite graduation est 10 fois plus petite que chaque grande graduation (et on a une moyenne graduation au milieu pour nous aider à nous repérer). Comme les grandes graduations sont des dixièmes, les petites graduations qui sont 10 fois plus petites sont donc des centièmes (je me rappelle unités - virgule - dixièmes - centièmes - millièmes).\
  \
  Je veux donc aller à 7,77 à partir de 7,6
  - 7,77 = 7 unités + 7 dixièmes + 7 centièmes
  - 7,6 = 7 unités + 6 dixièmes
  Pour aller de 7,6 à 7,77 il faut donc que j'ajoute 1 dixième et 7 centièmes, c'est à dire que j'avance de 1 grande graduation et de 7 petites graduations.
  #image("6N20-2.png")
]

#exemple(titre: "Exemple 2")[
  Placer le nombre 3,322 sur la demi-droite graduée ci-dessous.
  #image("6N20-3.png")
  #set text(couleurPrincipale)
  + Comme point de départ, on peut choisir 3,34 ou 3,38. Puisqu'on veut placer 3,322 on va choisir 3,34 qui est plus proche
  + Je remarque qu'il y a 3 grandes graduations entre 3,34 et 3,38 ; c'est certainement 3,35 ; 3,36 et 3,37. Ce qui signifie que chaque grande graduation vaut un centième.
  + Je remarque que l'espace entre chaque grande graduation est partagé en 10 petits espaces donc chaque petite graduation est 10 fois plus petite que chaque grande graduation (et on a une moyenne graduation au milieu pour nous aider à nous repérer). Comme les grandes graduations sont des centièmes, les petites graduations qui sont 10 fois plus petites sont donc des millièmes (je me rappelle unités - virgule - dixièmes - centièmes - millièmes).\
  \
  Je veux donc aller à 3,322 à partir de 3,34
  - 3,34 = 3 unités + 3 dixièmes + 4 centièmes
  - 3,322 = 3 unités + 3 dixièmes + 2 centièmes + 2 millièmes
  Pour aller de 3,34 à 3,332 il faut donc que j'enlève 2 centièmes et que j'ajoute 2 millièmes, c'est à dire que je recule de 2 grandes graduations et que j'avance de 2 petites graduations.
  #image("6N20-4.png")
]
