#regle(titre: "Convention d'écriture")[
  Si $p$ désigne un nombre, le quotient $p/100$ peut aussi s’écrire $p %$.
]

#exemples()[
  #block()[
    $ (12,435)/100 = 12,435 % $
  ]
  #block()[
    $ 3/8 = 3 ÷ 8 = 0,375 = (37,5)/100 = 37,5 % $
  ]
]
