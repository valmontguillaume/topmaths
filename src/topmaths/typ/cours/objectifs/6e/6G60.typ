#definitions()[
  #align(horizon,
    grid(
      columns: 2,
      row-gutter: 1em,
      column-gutter: 0.5em,
      align(center, image("6G60-1.png", height: 4em)), [Le #motDefini()[cube] est un solide uniquement constitué de carrés.],
      align(center, image("6G60-2.png", height: 4em)), [Le #motDefini()[pavé droit], qu’on appelle aussi #motDefini()[parallélépipède rectangle] est un solide uniquement constitué de rectangles.],
      align(center, image("6G60-3.png", height: 4em)), [Le #motDefini()[prisme droit] est un solide uniquement constitué de rectangles et de deux polygones superposables et parallèles qu’on appelle les #motDefini()[bases] du prisme.\
      Ici les bases sont des hexagones, c’est donc un prisme droit à base hexagonale.],
      align(center, image("6G60-4.png", height: 4em)), [La #motDefini()[pyramide] est un solide uniquement constitué de plusieurs triangles et d’un polygone qu’on appelle la #motDefini()[base] de la pyramide.\
      Ici la base est un carré, c’est donc une pyramide à base carrée.],
      align(center, image("6G60-5.png", height: 4em)), [Le #motDefini()[cylindre] est un solide uniquement constitué d’une surface latérale et de deux disques qu’on appelle les #motDefini()[bases] du cylindre.],
      align(center, image("6G60-6.png", height: 4em)), [Le #motDefini()[cône] est un solide uniquement constitué d’une surface latérale et d’un disque qu’on appelle la #motDefini()[base] du cône.],
      align(center, image("6G60-7.png", height: 4em)), [La #motDefini()[sphère] est l’équivalent du cercle dans l’espace.
      
      La #motDefini()[boule] est l’équivalent du disque dans l’espace.]
    )
  )
]

#definitions()[
  Un #motDefini()[polyèdre] est un solide dont les faces sont des polygones.

  Les côtés de ces polygones sont appelés #motDefini()[arêtes], ils sont délimités par des points appelés #motDefini()[sommets].
]

#exemple()[
  #image("6G60-8.png", height: 12em)
]

#remarques()[
  - Le cube, le parallélépipède rectangle, le prisme droit et la pyramide sont des polyèdres.
  - Le cylindre, le cône et la boule ne sont pas des polyèdres.
]
