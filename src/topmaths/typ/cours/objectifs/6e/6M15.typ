== Définitions

#definition()[
  Un #motDefini()[cube] est un solide dont les 6 faces sont des carrés.\
  Un #motDefini()[parallélépipède rectangle], qu'on appelle aussi un #motDefini()[pavé droit] est un solide dont les 6 faces sont des rectangles.
]

#exemples()[
  #box(image("6M15-1.png", width: 6em)) #box(image("6M15-2.png", width: 6em))
]

#definition()[
  Le #motDefini()[volume] d'un solide est la mesure de l'espace occupé par ce solide.
]

#exemple()[
  Calculer le volume du solide suivant :

  #box(width: 30%)[
    #image("6M15-3.png", width: 10em)
  ]

  #set text(couleurPrincipale)

  #box(width: 69%)[
    On remarque qu'il est constitué de plusieurs cubes d'arête $1 "cm"$ :
    - Il est constitué de $6$ couches
    - Chaque couche contient $3$ lignes
    - Chaque ligne contient $4$ cubes

    Une couche contient alors $3 "lignes" times 4 "cubes" = 12 "cubes"$.\
    Ce solide contient alors $6 "couches" times 12 "cubes" = 72 "cubes"$.

    Comme ce solide est constitué de $72$ cubes d'arête $1 "cm"$,\
    on dit qu'il a un volume de $72 "cm" ^3$ ($72$ centimètres cubes)
  ]
]

== Propriétés

#proprietes()[
  Volume d'un cube : $cal(V) = c times c times c$\
  Volume d'un pavé droit : $cal(V) = L times l times h$
]

#exemple(titre: "Exemple 1")[
  Calculer le volume du cube suivant :
  #image("6M15-4.png", height: 9em)

  #set text(couleurPrincipale)
  $cal(V) = c times c times c = 3 "cm" times 3 "cm" times 3 "cm" = 27 "cm"^3$
]

#exemple(titre: "Exemple 2")[
  Calculer le volume du pavé droit suivant :
  #image("6M15-5.png", height: 10em)

  #set text(couleurPrincipale)
  $cal(V) = L times l times h = 6 "cm" times 4 "cm" times 3 "cm" = 72 "cm"^3$
]


