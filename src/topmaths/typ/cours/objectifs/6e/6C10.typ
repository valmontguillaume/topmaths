Connaître les tables de multiplication est extrêmement important pour réussir en mathématiques.\
Un élève qui ne les connaît pas aura beaucoup de mal avec la moitié du programme alors il est très important de s'entraîner jusqu'à les connaître au moins jusqu'à 9.
#v(1em)
Pour pouvoir commencer à les apprendre, il faut déjà les avoir.
#block(breakable: false)[
  Des tables de multiplications sont disponibles sur la page #link("https://topmaths.fr/?v=student&ref=download")[Téléchargements] de la page #link("https://topmaths.fr/?v=student")[Outils pour les élèves] de #link("https://topmaths.fr/")[topmaths].
]
#block(breakable: false)[
  Une fois qu'on les a, il ne faut pas les apprendre l'une après l'autre comme une poésie mais essayer de se les rappeler dans le désordre.\
  Une fois que vous les connaissez à peu près, vous pouvez vous entraîner directement sur topmaths : #link("https://topmaths.fr/?uuid=challengeTables&v=exercise")[https://topmaths.fr/?uuid=challengeTables&v=exercise] ou sur l'application #link("https://www.multimaths.net/defitables.php")[Défi Tables].\
]
#block(breakable: false)[
  Enfin, pour y arriver ça ne sert à rien de passer des heures dessus d'affilée, il vaut mieux en faire un petit peu (quelques minutes) chaque jour !\
  Pour ça, mettre un rappel sur son téléphone est une très bonne idée. Tous les jours au début, puis quand ça devient trop facile mettre tous les trois jours, puis une semaine, deux semaines,...
]