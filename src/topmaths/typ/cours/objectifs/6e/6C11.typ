#methode()[
  Pour multiplier un nombre entier par 10, 100, 1 000... on ajoute le même nombre de zéros et pour diviser, on enlève le même nombre de zéros !
]

#let sp() = h(1mm)

#exemples()[
  - $#vert()[1#sp()740] times 1#rouge()[0#sp()000] = #vert()[1#sp()740] #rouge()[0#sp()000] = #vert()[17#sp()40]#rouge()[0#sp()000]$
  - $2#sp()900#sp()#rouge()[000] ÷ 1#sp()#rouge()[000] = 2#sp()900$
]