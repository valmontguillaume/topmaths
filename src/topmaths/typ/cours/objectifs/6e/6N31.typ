== Les fractions décimales
#vocabulaire()[
  Une #motDefini()[fraction] désigne le rapport entre deux nombres entiers, ou une division.\
  Par exemple pour désigner un rapport de 2 parts sur 3, on peut écrire $2/3$ #box(height: 1em, text(size: 0.8em)[ $<-$ numérateur\ $<-$ dénominateur])
]

#definition()[
  Une #motDefini()[fraction décimale] est une fraction de dénominateur 10, 100, 1 000, …
]

#exemples()[
  #table(
    columns: 9,
    stroke: none,
    align: horizon + center,
    $ 1/10 $, [;], $ 1/100 $, [;], $ 1/1000 $, [;], $ 23/10 $, [;], $ 15/1000 $
  )
]

#propriete()[
  Une fraction décimale admet plusieurs écritures.
]

#exemple()[
  #table(
    columns: 9,
    stroke: none,
    align: horizon + center,
    $ 25/10 $, [=], $ frac(25 #rouge()[0],10#rouge()[0]) $, [=], $ frac(25 #rouge()[00],10#rouge()[00]) $
  )
]

== Les nombres décimaux

#definition()[
  Un #motDefini()[nombre décimal] est un nombre qui peut s'écrire sous forme d'une fraction décimale.\
  Un nombre décimal admet une écriture à virgule appelée #motDefini()[écriture décimale].
]

#exemples()[
  $3,45$ est un nombre décimal car il peut s’écrire $345/100$.\
  #v(0pt)
  $15$ est un nombre décimal car il peut s'écrire $150/10$.
]

#definitions()[
  La partie à gauche de la virgule d'un nombre décimal est sa #motDefini()[partie entière].\
  Zéro virgule la partie à droite de la virgule d'un nombre décimal est sa #motDefini()[partie décimale].
]

#exemple()[
  La partie entière de 27,31 est 27.\
  La partie décimale de 27,31 est 0,31.
]

#propriete()[
  Un nombre est la somme de sa partie entière et de sa partie décimale.
]

#exemple()[
  Dans l'exemple précédent, si on fait partie entière (27) + partie décimale (0,31), on obtient bien 27,31.
]