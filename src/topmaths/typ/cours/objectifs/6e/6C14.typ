#exemple()[
  #table(
    columns: 5,
    stroke: none,
    align: center + horizon,
    box(width: 50pt, image("6C14-1.png")), [+], box(width: 50pt, image("6C14-2.png")), [=], box(width: 50pt, image("6C14-3.png")),
    $ 1/4 $, [+], $ 2/4 $, [=], $ 3/4 $
  )
]

#vocabulaire()[
  Dans une fractions, le nombre du haut est appelé #motDefini()[numérateur] tandis que le nombre du bas est appelé #motDefini()[dénominateur]
]

#methode()[
  #set list(marker: [--])
  Pour additionner ou soustraire deux fractions de même dénominateur :
  - on additionne ou on soustrait leurs numérateurs ;
  - on garde le même dénominateur
]

#exemples()[
  $ 3/7 + 5/7 = 8/7 $
  $ 11/15 - 4/15 = 7/15 $
]
