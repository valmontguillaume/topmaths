#methode(titre: "Méthode pour construire un carré")[
  + Tracer un côté
  + Tracer deux demi-droites perpendiculaires à ce côté passant par chacune de ses deux extrémités
  + Mesurer la même longueur sur les deux demi-droites tracées
  + Terminer le carré
  + Le coder
]

#exemple()[
  #image("6G25-1.png", width: 90%)
]

#methode(titre: "Méthode pour construire un rectangle")[
  + Tracer une longueur (un grand côté)
  + Tracer deux demi-droites perpendiculaires à ce côté passant par chacune de ses deux extrémités
  + Mesurer la largeur du rectangle (longueur du petit côté) sur les deux demi-droites tracées
  + Terminer le rectangle
  + Le coder
]

#exemple()[
  #image("6G25-2.png")
]
