#definitions()[
  Un repère du plan est formé par deux droites graduées de même origine (le même "0").\
  La première est appelée #rouge()[l’axe des abscisses] (elle est souvent horizontale).\
  La deuxième est appelée #vert()[l’axe des ordonnées] (elle est souvent verticale).\
  Quand ces deux droites sont perpendiculaires, on dit que le repère est orthogonal (ce sera toujours le cas au collège).
]

#definitions()[
  Dans un repère du plan, chaque point est repéré par deux nombres relatifs qu’on appelle ses #noir()[coordonnées].\
  Le premier est l’#rouge()[abscisse], le second est l’#vert()[ordonnée].\
  On les note toujours dans cet ordre : #noir()[(#rouge()[abscisse] ; #vert()[ordonnée])]
]

#exemple()[
  #show: normal
  #place(right, image("5N35-1.png"), dx: 110%)
  L’#rouge()[abscisse] du point A est #rouge()[2]\
  L’#vert()[ordonnée] du point A est #vert()[3]\
  Les #noir()[coordonnées] du point A se notent #noir()[(#rouge()[2] ; #vert()[3])]\
  Le point B a pour #noir()[coordonnées] #noir()[(#rouge()[-3] ; #vert()[0])]\
  Les #noir()[coordonnées] du point C sont #noir()[(#rouge()[0] ; #vert()[-2])]\
  D #noir()[(#rouge()[-3] ; #vert()[-2])]
]