#block(breakable: false)[
  == Reconnaître des angles de même mesure
  #propriete()[
    Si deux droites parallèles sont coupées par une sécante, alors les angles correspondants qu'elles forment ont la même mesure.
    #implication(image("5G31-1.png"), image("5G31-2.png"))
  ]

  #propriete()[
    Si deux droites parallèles sont coupées par une sécante, alors les angles alternes-internes qu'elles forment ont la même mesure.
    #implication(image("5G31-1.png"), image("5G31-3.png"))
  ]
]

#block(breakable: false)[
  == Reconnaître des droites parallèles
  #propriete()[
    Si deux droites coupées par une sécante forment deux angles correspondants de même mesure, alors ces droites sont parallèles.
    #implication(image("5G31-2.png"), image("5G31-1.png"))
  ]
  #propriete()[
    Si deux droites coupées par une sécante forment deux angles alternes-internes de même mesure, alors ces droites sont parallèles.
    #implication(image("5G31-3.png"), image("5G31-1.png"))
  ]
]

#remarques()[
  On dit que ces propriétés sont les réciproques des propriétés énoncées au 1.\
  Les données et la conclusion d'une propriété et de sa réciproque sont échangées.
]