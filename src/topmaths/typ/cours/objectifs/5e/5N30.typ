#definitions()[
  On a vu que – 8 + 8 = 0, que – 5 + 5 = 0, que + 4 – 4 = 0 etc.\
  On dit que – 8 est #rouge()[l’opposé] de + 8 et que + 8 est #rouge()[l’opposé] de – 8, ou encore que + 8 et – 8 sont #rouge()[opposés].\
  Les nombres positifs et négatifs sont appelés « #rouge()[nombres relatifs] », ils sont écrits avec un signe + ou – et un nombre que l’on appelle la #rouge()[valeur absolue].
]

#remarque()[
  Sur une droite graduée, un nombre et son opposé sont "symétriques" par rapport à 0.
]