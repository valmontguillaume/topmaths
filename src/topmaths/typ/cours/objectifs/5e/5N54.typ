#methode()[
  Pour calculer la valeur d'une expression littérale, on commence par la recopier en remplaçant les lettres par les valeurs données dans l'énoncé puis on calcule.
]

#exemple()[
  Calculer $3x + 6$ pour $x = 5$.

  #set text(couleurPrincipale)
  $3x + 6 = 3 #rouge()[$times$] #vert()[$x$] + 6$ #rouge()[(comme il n'y a rien entre le $3$ et le $x$, on comprend qu'il y a un $times$ sous-entendu)]
  
  $#phantom()[$3x + 6$] = 3 times #vert()[$5$] + 6$ #vert()[(on remplace $x$ par $5$)]
  
  $#phantom()[$3x + 6$] = 15 + 6$
  
  $#phantom()[$3x + 6$] = 21$
]