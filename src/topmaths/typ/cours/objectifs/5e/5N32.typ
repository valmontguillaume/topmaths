#exemples()[
  #normal()[
    Différents cas sont possibles :
    - On ajoute deux nombres positifs\
    #vert()[\+ 7] + (#vert()[\+ 2]) = 7 + 2 = 9 = #vert()[\+ 9]\
    #vert()[\+ 7] + (#vert()[\+ 10]) = 7 + 10 = 17 = #vert()[\+ 17]

    - On ajoute deux nombres négatifs
    #rouge()[\– 7] + (#rouge()[\– 2]) = –7 – 2 = #rouge()[\– 9]\
    #rouge()[\– 7] + (#rouge()[\– 10]) = –7 – 10 = #rouge()[\– 17]

    - On ajoute un négatif et un positif
    #rouge()[\– 7] + (#vert()[\+ 2]) = –7 + 2 = #rouge()[\– 5]\
    #rouge()[\– 7] + (#vert()[\+ 10]) = –7 + 10 = #vert()[\+ 3] = 3

    - On ajoute un positif et un négatif
    #vert()[\+ 7] + (#rouge()[\– 2]) = 7 – 2 = #vert()[\+ 5] = 5\
    #vert()[\+ 7] + (#rouge()[\– 10]) = 7 – 10 = #rouge()[\– 3]
  ]
]

#propriete(titre: "Règle de calcul de la somme de deux relatifs")[
  Si on ajoute deux relatifs de #rouge()[même signe], leur somme est le relatif de #rouge()[même signe] qui a pour valeur absolue #vert()[la somme des valeurs absolues].\
  Si on ajoute deux relatifs de #rouge()[signes différents], leur somme est le relatif de signe #rouge()[le signe de celui qui a la plus grande valeur absolue] et de valeur absolue #vert()[la différence des valeurs absolues].
]

#remarque(titre: "Deux remarques importantes")[
1\. l’addition des relatifs est #rouge()[commutative] : on ne change pas la valeur de la somme lorsqu’on change l’ordre des termes d’une addition :\
#rouge()[\– 15] + (#vert()[\+ 10]) = #vert()[\+ 10] + (#rouge()[\– 15]) = 10 – 15 = – 5

2\. on s’aperçoit que :\
\+ 7 #gris()[\+ \(]+ 4#gris()[\)] = 7 + 4\
\+ 7 #gris()[\+ \(]– 4#gris()[\)] = 7 – 4\
\– 7 #gris()[\+ \(]+ 4#gris()[\)] = – 7 + 4\
\– 7 #gris()[\+ \(]– 4#gris()[\)] = – 7 – 4\
Dans une somme de relatifs, il est plus simple de supprimer les parenthèses et les signes + d’addition afin de pouvoir calculer comme dans les programmes de calcul.
]
