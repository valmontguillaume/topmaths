#remarques()[
  En 6ème, on a placé des points sur une demi-droite graduée : #box(image("5N34-1.png", height: 1em))

  Maintenant qu'on connaît aussi les nombres négatifs, on peut placer des points sur une droite graduée (dans les deux sens !) : #box(image("5N34-2.png", height: 1em))

  Les nombres négatifs sont le "miroir" des nombres positifs.
]

#definition()[
  Le nombre qui sert à repérer un point sur une droite graduée s'appelle son #motDefini()[abscisse]
]

#exemple()[
  #image("5N34-3.png", height: 3em)
  L'abscisse du point A est - 3,1\
  L'abscisse du point B est - 1,8\
  L'abscisse du point C est 1,3
]