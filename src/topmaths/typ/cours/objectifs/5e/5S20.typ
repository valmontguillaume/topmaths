#definition(titre: "Définitions-Exemples")[
  Une #motDefini()[expérience aléatoire] est une expérience qui dépend du hasard : on ne peut pas prévoir à l’avance le résultat. #noir()[Exemple : Lancer un dé et regarder sur quel numéro il va s'arrêter]\
  Les #motDefini()[issues] d’une expérience aléatoire sont les différents résultats possibles de cette expérience. #noir[Exemple : un dé peut s'arrêter sur le numéro 1, le numéro 2, etc.]\
  La #motDefini()[probabilité] d’une issue peut s’interpréter comme la "proportion de chance" d’obtenir cette issue. #noir()[Exemple : Comme il y a 1 face portant le numéro 2 sur un total de 6 faces, la probabilité de tomber sur une face qui porte le numéro 2 est $1/6$]\
  Un #motDefini()[événement] est constitué d’issues. #noir()[Exemple : L'événement "Obtenir un nombre pair" est constitué des issues "tomber sur le numéro 2", "tomber sur le numéro 4" et "tomber sur le numéro 6"]\
  On dit qu’un événement est #motDefini()[réalisé] lorsqu’on a obtenu l’une de ses issues. #noir()[Exemple : Si je tombe sur le numéro 2, l'événement "Tomber sur un nombre pair est réalisé" mais l'événement "Tomber sur un nombre plus grand que 3" n'est pas réalisé.]\
  On dit qu’un événement est #motDefini()[impossible] s’il ne peut pas se produire. #noir()[Exemple : "Tomber sur un nombre plus grand que 10"]\
  On dit qu’un événement est #motDefini()[certain] s’il se produit toujours. #noir()[Exemple : "Tomber sur un nombre plus petit que 100"]
]

#remarque()[
  Pour dire à quel point on estime qu'un phénomène a des chances de se produire ou pas on peut les placer sur ce qu'on appelle une échelle de probabilité.

  Tout à gauche il y a les phénomènes qu'on pense impossibles et tout à droite les phénomènes dont on pense qu'ils vont se réaliser à coup sûr (on dit qu'ils sont certains) avec un dégradé entre les deux.

  #image("5S20-1.png")
]
