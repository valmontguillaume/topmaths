#proprietes()[
  Un nombre entier est divisible :
  - par #rouge()[2] lorsque son #noir()[chiffre des unités] est #vert()[0, 2, 4, 6 ou 8] ;
  - par #rouge()[5] lorsque son #noir()[chiffre des unités] est #vert()[0 ou 5] ;
  - par #rouge()[10] lorsque son #noir()[chiffre des unités] est #vert()[0].
]

#exemples()[
  - 250 est divisible par 2, par 5 et par 10.
  - 75 est divisible par 5.
]

#proprietes()[
  Un nombre entier est divisible :
  - par #rouge()[3] lorsque la #noir()[somme de ses chiffres] est divisible par #vert()[3] ;
  - par #rouge()[9] lorsque la #noir()[somme de ses chiffres] est divisible par #vert()[9].
]

#exemples()[
  - 2022 est divisible par 3. En effet : 2 + 0 + 2 + 2 =  6 et 6 est divisible par 3.
  - 1845 est divisible par 9. En effet : 1 + 8 + 4 + 5 = 18 et 18 est divisible par 9.
]
