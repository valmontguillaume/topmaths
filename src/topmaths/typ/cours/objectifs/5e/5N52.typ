#remarque()[
  On connaît déjà quelques formules, par exemple :
  -  $"aire d’un rectangle" = L times l$
  -  $"périmètre d’un cercle" = 2 π r$
  Dans cette séquence, on verra qu’on peut aussi se créer ses propres formules ! (mais on préférera les appeler des "expressions" plutôt que des "formules")
]

#exemple(titre: "Exemple 1")[
  $n$ désigne un nombre entier.\
  Exprimer l’entier suivant en fonction de $n$ (c’est comme ça qu’on demande d’écrire la "formule" qui permet de calculer l’entier suivant à partir de $n$).

  #set text(couleurPrincipale)
  Si je pars d’un entier et que je cherche à aller à l’entier suivant, je dois ajouter $1$.\
  Si je pars d’un entier $n$ et que j’ajoute $1$, j’obtiens $n + 1$.\
  L’entier suivant est donné par l’expression (la formule) $n + 1$.
]

#exemple(titre: "Exemple 2")[
  $n$ désigne un nombre entier.\
  Exprimer le triple en fonction de $n$

  #set text(couleurPrincipale)
  Pour obtenir le triple d’un nombre, je dois multiplier par $3$.\
  Pour obtenir le triple d’un nombre $n$, je le multiplie par $3$ et j’obtiens $n times 3 = 3 n$.\
  Le triple de $n$ est donné par l’expression $3 n$.
]
