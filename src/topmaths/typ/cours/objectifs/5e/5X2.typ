#definition()[
  On décide de considérer – 1, – 2, – 3 ... comme de nouveaux nombres.\
  Ils sont précédés d’un signe "–" et on les appelle "#motDefini()[nombres négatifs]".
]

#remarques()[
  #vert()[\– 5 + 5] – 1= – 1\
  Donc : #vert()[0] – 1= – 1

  #vert()[\+ 4 – 4] + 2 = + 2\
  Donc : #vert()[0] + 2 = + 2\
  Or, on sait que #rouge()[0 + 2] = #rouge()[2]\
  On peut donc en déduire que #rouge()[+2] = #rouge()[2]
]

#definitions()[
  Les nombres entiers peuvent donc être notés avec un signe +, on les appelle des #motDefini()[nombres positifs].\
  Les nombres positifs et les nombres négatifs forment ce qu'on appelle les #motDefini()[nombres relatifs].
]

#exemples()[
  $+3 = 3$\
  $5 = +5$
]

#remarque()[
  Le nombre 0 est le seul nombre à la fois positif et négatif.
]