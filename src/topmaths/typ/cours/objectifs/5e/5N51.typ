#propriete()[
    $a$ et $b$ désignent des nombres.\
    $a #vert("x") + b #vert("x") = (a + b)#vert("x")$
]

#exemples()[
  $C = 2#vert("x") + 3#rouge("x")$\
  $C = #vert("x") + #vert("x") + #rouge("x") + #rouge("x") + #rouge("x")$ \
  $C = 5x$

  $D = 4#vert($a$) + 7#rouge($b$) – 3#vert($a$) + 2#rouge($b$)$\
  $D = 4#vert($a$) – 3#vert($a$) + 7#rouge($b$) + 2#rouge($b$)$\
  $D = #vert($a$) + 9#rouge($b$)$
]