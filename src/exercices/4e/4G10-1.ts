import TransformationsDuPlanEtCoordonnees from '../3e/3G10-1'
export const titre = 'Trouver les coordonnées de l\'image d\'un point par une translation et une rotation'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const dateDePublication = '28/10/2021'
export const dateDeModifImportante = '28/12/2022'
export const uuid = '7b40c'
export const refs = {
  'fr-fr': ['4G10-1'],
  'fr-ch': ['10ES2-2']
}
export default class TransformationsDuPlanEtCoordonnees4e extends TransformationsDuPlanEtCoordonnees {
  constructor () {
    super()
    this.sup = '3-4'
  }
}
