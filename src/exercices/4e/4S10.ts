import ConstruireUnDiagramme from '../5e/5S12'
export const titre = 'Construire un diagramme'
export const interactifReady = false
export const uuid = '26ea7'
export const refs = {
  'fr-fr': ['4S10'],
  'fr-ch': ['10FA5-1']
}
export default class ConstruireUnDiagramme4e extends ConstruireUnDiagramme {
  constructor () {
    super()
    this.sup = 3
    this.sup2 = 2
    this.sup3 = 1
  }
}
