import FractionsIrreductibles from '../3e/3A11'
export const titre = 'Fractions irréductibles'
export const interactifReady = false
export const uuid = '426f5'
export const refs = {
  'fr-fr': ['4A11-2'],
  'fr-ch': []
}
export default class FractionsIrreductibles4e extends FractionsIrreductibles {
}
