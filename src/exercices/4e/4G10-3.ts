import ImagePtParTranslation from '../2e/2G23-1'
export const titre = 'Déterminer graphiquement des images par des translations'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDePublication = '15/10/2023'
export const uuid = '6ddc5'
export const refs = {
  'fr-fr': ['4G10-3'],
  'fr-ch': ['10ES2-4']
}
export default class ImagePtParTranslation4e extends ImagePtParTranslation {
  constructor () {
    super()
    this.classe = 4
    this.sup = '4'
    this.nbQuestions = 3
  }
}
