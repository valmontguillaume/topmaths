import decompositionFacteursPremiers from '../3e/3A10-3'
export const titre = 'Décomposer un nombre entier en produit de facteurs premiers'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCOpen'
export const uuid = 'b8a38'
export const refs = {
  'fr-fr': ['4A11-1'],
  'fr-ch': []
}
export default class DecompositionFacteursPremiers4e extends decompositionFacteursPremiers {
}
