import TesterSiUnNombreEstSolutionDUneEquation from './4L14-0'
export const titre = 'Tester si un nombre est solution d\'une équation du second degré'
export const interactifReady = false
export const uuid = '1188b'
export const refs = {
  'fr-fr': ['4L14-2'],
  'fr-ch': ['10FA3-4', '11FA6-2']
}
export default class TesterSiUnNombreEstSolutionDUneEquationDeg2 extends TesterSiUnNombreEstSolutionDUneEquation {
  constructor () {
    super()
    this.sup2 = '9-6-7'
    this.nbQuestions = 3
  }
}
