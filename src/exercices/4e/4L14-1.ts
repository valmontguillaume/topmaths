import TesterSiUnNombreEstSolutionDUneEquation from './4L14-0'
export const titre = 'Tester si un nombre est solution d\'une équation du premier degré'
export const interactifReady = false
export const uuid = '5ecb8'
export const refs = {
  'fr-fr': ['4L14-1'],
  'fr-ch': ['10FA3-3']
}
export default class TesterSiUnNombreEstSolutionDUneEquationDeg1 extends TesterSiUnNombreEstSolutionDUneEquation {
  constructor () {
    super()
    this.sup2 = '1-3-5-2-4'
  }
}
