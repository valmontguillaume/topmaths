import ArrondirUneValeur4e from './4G20-4'
export const titre = 'Arrondir une valeur comprenant un cosinus'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDePublication = '02/07/2021'
export const uuid = '2f4d7'
export const refs = {
  'fr-fr': ['4G40-2'],
  'fr-ch': []
}
export default class ArrondirUneValeur4eme extends ArrondirUneValeur4e {
  constructor () {
    super()
    this.version = 2
    this.spacing = 3
  }
}
