import { combinaisonListes, shuffle } from '../../lib/outils/arrayOutils'
import { texteEnCouleur } from '../../lib/outils/embellissements'
import Exercice from '../Exercice'
import { mathalea2d, colorToLatexOrHTML } from '../../modules/2dGeneralites'
import { listeQuestionsToContenu } from '../../modules/outils'
import { scratchblock } from '../../modules/scratchblock'
import { allerA, avance, baisseCrayon, creerLutin, leveCrayon, tournerD } from '../../modules/2dLutin'

export const titre = 'Dessiner avec scratch'

/**
 * * Dessiner selon un programme scratch
 * * 4Algo1-0
 * @author Sébastien Lozano
 * mise à plat du big ouaille suite au passage à la V2
 * implémentation fonction scratchblock par Jean-Claude Lhote
 * la fonction gère la sortie Latex ou html du code scratch
 */

export const uuid = '33c9a'

export const refs = {
  'fr-fr': ['4I1'],
  'fr-ch': []
}
export default class TracerAvecScratch extends Exercice {
  constructor () {
    super()
    this.consigne = 'Laquelle des 4 figures ci-dessous va être tracée avec le script fourni ?'
    this.typeExercice = 'Scratch'
    this.nbQuestions = 3
  }

  nouvelleVersion () {
    const typesDeQuestionsDisponibles = [1, 2, 3, 4, 5]

    const fenetreMathalea2D = { xmin: -10, ymin: -15, xmax: 60, ymax: 2, pixelsParCm: 10, scale: 0.2 }
    const pixelsParCm = fenetreMathalea2D.pixelsParCm * 5 / 100
    //    var unitesLutinParCm = 100;

    const listeTypeDeQuestions = combinaisonListes(typesDeQuestionsDisponibles, this.nbQuestions) // Tous les types de questions sont posées mais l'ordre diffère à chaque "cycle"
    // let listeTypeDeQuestions = combinaisonListesSansChangerOrdre(typesDeQuestionsDisponibles, this.nbQuestions) // Tous les types de questions sont posées --> à remettre comme ci-dessus

    for (let i = 0, texte, texteCorr, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      // une fonction pour gérer la sortie HTML/LaTeX
      // code est un string contenant le code svg ou tikz

      // une fonction pour dire le nom du polygone
      const myPolyName = (n:number) => {
        const sortie = {
          name: '',
          nameParSommets: '',
          nbPas: 0
        }
        switch (n) {
          case 2:
            sortie.name = 'segment'
            sortie.nameParSommets = 'AB'
            sortie.nbPas = 400
            break
          case 3:
            sortie.name = 'triangle équilatéral'
            sortie.nameParSommets = 'ABC'
            sortie.nbPas = 400
            break
          case 4:
            sortie.name = 'carré'
            sortie.nameParSommets = 'ABCD'
            sortie.nbPas = 400
            break
          case 5:
            sortie.name = 'pentagone régulier'
            sortie.nameParSommets = 'ABCDE'
            sortie.nbPas = 300
            break
          case 6:
            sortie.name = 'hexagone régulier'
            sortie.nameParSommets = 'ABCDEF'
            sortie.nbPas = 250
            break
          case 7:
            sortie.name = 'heptagone régulier'
            sortie.nameParSommets = 'ABCDEFG'
            sortie.nbPas = 200
            break
          case 8:
            sortie.name = 'octogone régulier'
            sortie.nameParSommets = 'ABCDEFGH'
            sortie.nbPas = 200
            break
          case 9:
            sortie.name = 'ennéagone régulier'
            sortie.nameParSommets = 'ABCDEFGHI'
            sortie.nbPas = 200
            break
        }
        return sortie
      }

      // une fonction pour renvoyer une situation
      // n définit le nombre de côtés du polygone régulier
      const mySituation = (n:number) => {
        const situations = [
          { // polygones réguliers
            nbCotes: n,
            nom: myPolyName(n).name,
            codeScratch: `\\begin{scratch}
\\blockinit{quand \\greenflag est cliqué}
\\blockpen{stylo en position d'écriture}
\\blockrepeat{répéter \\ovalnum{${n}} fois}
{
\\blockmove{avancer de \\ovalnum{${myPolyName(n).nbPas}} pas}
\\blockmove{tourner \\turnright{} de \\ovaloperator{\\ovalnum{360}/\\ovalnum{${n}}} degrés}
}
\\end{scratch}`,
            fig: '',
            fig_corr: ''
          }
        ]

        let tabAbsDemLutin2
        if (n === 6) {
          tabAbsDemLutin2 = [0, 3 * myPolyName(n).nbPas, 6 * myPolyName(n).nbPas, 9 * myPolyName(n).nbPas]
        } else if (n === 8) {
          tabAbsDemLutin2 = [0, 4 * myPolyName(n).nbPas, 8 * myPolyName(n).nbPas, 12 * myPolyName(n).nbPas]
        } else {
          tabAbsDemLutin2 = [0, 2 * myPolyName(n).nbPas, 4 * myPolyName(n).nbPas, 6 * myPolyName(n).nbPas]
        }
        // on mélange tout ça !
        tabAbsDemLutin2 = shuffle(tabAbsDemLutin2)
        // Les figures de l'énoncé
        // le lutin2  trace le cadre en pointillés
        const lutin2 = creerLutin()
        lutin2.color = colorToLatexOrHTML('black')
        lutin2.pointilles = 5
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin2)
        baisseCrayon(lutin2)
        allerA(fenetreMathalea2D.xmax * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin2)
        allerA(fenetreMathalea2D.xmax * pixelsParCm, fenetreMathalea2D.ymin * pixelsParCm, lutin2)
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymin * pixelsParCm, lutin2)
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin2)
        leveCrayon(lutin2)
        // le lutin2 fait la bonne figure
        lutin2.pointilles = 0
        lutin2.color = colorToLatexOrHTML('blue')
        allerA(tabAbsDemLutin2[0], 0, lutin2)
        baisseCrayon(lutin2)
        for (let k = 1; k < n + 1; k++) {
          avance(myPolyName(n).nbPas, lutin2)
          tournerD(360 / n, lutin2)
        }
        // le lutin2 fait un polygone régulier avec un côté de plus
        leveCrayon(lutin2)
        allerA(tabAbsDemLutin2[1], 0, lutin2)
        baisseCrayon(lutin2)
        for (let k = 1; k < n + 1 + 1; k++) {
          avance(myPolyName(n + 1).nbPas, lutin2)
          tournerD(360 / (n + 1), lutin2)
        }

        // le lutin2 fait un polygone régulier avec un côté de moins
        leveCrayon(lutin2)
        allerA(tabAbsDemLutin2[2], 0, lutin2)
        baisseCrayon(lutin2)
        for (let k = 1; k < n; k++) {
          avance(myPolyName(n - 1).nbPas, lutin2)
          tournerD(360 / (n - 1), lutin2)
        }

        // le lutin2 fait une figure ouverte à n côtés
        leveCrayon(lutin2)
        allerA(tabAbsDemLutin2[3], 0, lutin2)
        baisseCrayon(lutin2)
        for (let k = 1; k < n + 1; k++) {
          avance(myPolyName(n).nbPas, lutin2)
          tournerD((360 / n) - 10, lutin2)
        }
        allerA(tabAbsDemLutin2[3], 0, lutin2)

        const mesAppelsEnonce = [
          lutin2
        ]
        situations[0].fig = mathalea2d(
          fenetreMathalea2D,
          mesAppelsEnonce
        )

        // les figures de la correction
        // le lutin3  trace le cadre
        const lutin3 = creerLutin()
        lutin3.color = colorToLatexOrHTML('black')
        lutin3.pointilles = 5
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin3)
        baisseCrayon(lutin3)
        allerA(fenetreMathalea2D.xmax * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin3)
        allerA(fenetreMathalea2D.xmax * pixelsParCm, fenetreMathalea2D.ymin * pixelsParCm, lutin3)
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymin * pixelsParCm, lutin3)
        allerA(fenetreMathalea2D.xmin * pixelsParCm, fenetreMathalea2D.ymax * pixelsParCm, lutin3)
        leveCrayon(lutin3)
        // le lutin3 fait la bonne figure
        lutin3.pointilles = 0
        lutin3.color = colorToLatexOrHTML('green')
        allerA(tabAbsDemLutin2[0], 0, lutin3)
        baisseCrayon(lutin3)
        for (let k = 1; k < n + 1; k++) {
          avance(myPolyName(n).nbPas, lutin3)
          tournerD(360 / n, lutin3)
        }
        // le lutin3 fait un polygone régulier avec un côté de plus
        lutin3.color = colorToLatexOrHTML('red')
        leveCrayon(lutin3)
        allerA(tabAbsDemLutin2[1], 0, lutin3)
        baisseCrayon(lutin3)
        for (let k = 1; k < n + 1 + 1; k++) {
          avance(myPolyName(n + 1).nbPas, lutin3)
          tournerD(360 / (n + 1), lutin3)
        }

        // le lutin3 fait un polygone régulier avec un côté de moins
        leveCrayon(lutin3)
        allerA(tabAbsDemLutin2[2], 0, lutin3)
        baisseCrayon(lutin3)
        for (let k = 1; k < n; k++) {
          avance(myPolyName(n - 1).nbPas, lutin3)
          tournerD(360 / (n - 1), lutin3)
        }

        // le lutin3 fait une figure ouverte à n côtés
        leveCrayon(lutin3)
        allerA(tabAbsDemLutin2[3], 0, lutin3)
        baisseCrayon(lutin3)
        for (let k = 1; k < n + 1; k++) {
          avance(myPolyName(n).nbPas, lutin3)
          tournerD((360 / n) - 10, lutin3)
        }
        allerA(tabAbsDemLutin2[3], 0, lutin3)

        const mesAppelsCorr = [
          lutin3
        ]
        situations[0].fig_corr = mathalea2d(
          fenetreMathalea2D,
          mesAppelsCorr
        )

        const enonces = []
        enonces.push({
          enonce: `
          ${scratchblock(situations[0].codeScratch)}
          <br>
          ${situations[0].fig}
          `,
          question: '',
          correction: `
          Les figures rouges sont erronées.
          <br> La figure tracée par le programme a ${situations[0].nbCotes} côtés de même longueur et ${situations[0].nbCotes} angles de même mesure, c'est un ${situations[0].nom}.
          <br>${texteEnCouleur('La bonne figure est donc la figure verte.')}
          <br><br>
          ${situations[0].fig_corr}
          `
        })

        return enonces
      }

      const enonces = []
      enonces.push(mySituation(3)[0])
      enonces.push(mySituation(4)[0])
      enonces.push(mySituation(5)[0])
      enonces.push(mySituation(6)[0])
      enonces.push(mySituation(8)[0])
      texte = `${enonces[listeTypeDeQuestions[i] - 1].enonce}`
      texteCorr = `${enonces[listeTypeDeQuestions[i] - 1].correction}`

      if (this.listeQuestions.indexOf(texte) === -1) { // Si la question n'a jamais été posée, on en créé une autre
        this.listeQuestions[i] = texte
        this.listeCorrections[i] = texteCorr
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }
}
