import APartirDeDivisionsEuclidiennes from '../6e/6C11-3'
export const titre = 'Indiquer une égalité à partir d\'une division euclidienne'
export const amcReady = true
export const amcType = 'AMCOpen'
export const interactifReady = true
export const interactifType = 'mathLive'

export const dateDePublication = '14/09/2022'

/**
 * À partir de divisions euclidiennes, indiquer l'égalité fondamentale correspondante.
 * Ref 3A13-4
 *
 * @author Eric Elter
 */

export const uuid = 'b0cee'

export const refs = {
  'fr-fr': ['3A13-4'],
  'fr-ch': []
}
export default class APartirDeDivisionsEuclidiennes3e extends APartirDeDivisionsEuclidiennes {
  constructor () {
    super()
    this.classe = 3
  }
}
