import LireAbscisseDecimaleTroisFormes from '../6e/6N23-2'
export const titre = 'Lire abscisse décimale sous trois formes'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const uuid = '2fa3b'
export const refs = {
  'fr-fr': ['c3N22'],
  'fr-ch': []
}
export default class LireAbscisseDecimaleTroisFormesCM extends LireAbscisseDecimaleTroisFormes {
  constructor () {
    super()
    this.niveau = 'CM'
    this.sup = 1
  }
}
