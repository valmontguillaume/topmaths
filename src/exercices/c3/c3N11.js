import LireAbscisseEntiere2d from '../6e/6N11'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const titre = 'Lire l\'abscisse entière d\'un point (grands nombres)'
export const dateDeModifImportante = '26/08/2024'
export const uuid = 'c0fb1'
export const refs = {
  'fr-fr': ['c3N11'],
  'fr-ch': []
}
export default class LireAbscisseEntiere2dC3 extends LireAbscisseEntiere2d {
  constructor () {
    super()
    this.sup = 1
  }
}
