import NotationScientifique from '../4e/4C32'
export const titre = 'Associer un nombre décimal à sa notation scientifique'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
export const uuid = 'c9404'
export const refs = {
  'fr-fr': ['2N31-1'],
  'fr-ch': []
}
export default class NotationScientifique2e extends NotationScientifique {
}
