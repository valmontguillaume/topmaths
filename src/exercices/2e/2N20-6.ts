import listerDiviseursParDecompositionFacteursPremiers from '../3e/3A10-4'
export const titre = 'Compter/lister les diviseurs d\'un entier à partir de sa décomposition en facteurs premiers'
export const dateDeModifImportante = '02/06/2023'
export const interactifReady = false
export const uuid = '74939'
export const refs = {
  'fr-fr': ['2N20-6'],
  'fr-ch': []
}
export default class ListerDiviseursParDecompositionFacteursPremiers2nde extends listerDiviseursParDecompositionFacteursPremiers {
  constructor () {
    super()
    this.sup = true
  }
}
