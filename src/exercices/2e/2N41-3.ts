import DevelopperIdentitesRemarquables3 from '../3e/3L12-1'
export const titre = 'Développer $(a-b)(a+b)$'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcType = 'AMCHybride'
export const amcReady = true
export const uuid = '3b7ee'
export const refs = {
  'fr-fr': ['2N41-3'],
  'fr-ch': ['11FA2-9']
}
export default class DevelopperIdentitesRemarquables32nde extends DevelopperIdentitesRemarquables3 {
}
