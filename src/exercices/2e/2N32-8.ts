import CalculValeurApprocheeRacineCarree from '../4e/4G20-6'
export const titre = 'Encadrer une racine carrée'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const dateDePublication = '09/05/2023'
export const uuid = '99c84'

export const refs = {
  'fr-fr': ['2N32-8'],
  'fr-ch': []
}
export default class CalculValeurApprocheeRacineCarreeS extends CalculValeurApprocheeRacineCarree {
}
