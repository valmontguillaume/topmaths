import FormeLitteraleIntroduireUneLettre from '../4e/4L13-1'
export const titre = 'Produire une forme littérale en introduisant une lettre pour désigner une valeur inconnue'
export const interactifReady = false
export const uuid = '27741'
export const refs = {
  'fr-fr': ['2N50-2'],
  'fr-ch': []
}
export default class FormeLitteraleIntroduireUneLettre2nde extends FormeLitteraleIntroduireUneLettre {
}
