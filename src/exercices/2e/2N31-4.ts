import PrioritesEtRelatifsEtPuissances from '../4e/4C34'
export const titre = 'Calculer en utilisant les priorités opératoires et les puissances'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybrides'
export const uuid = '6fda8'
export const refs = {
  'fr-fr': ['2N31-4'],
  'fr-ch': []
}
export default class PrioritesEtRelatifsEtPuissances2e extends PrioritesEtRelatifsEtPuissances {
}
