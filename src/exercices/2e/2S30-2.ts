import FonctionsProbabilite1 from '../5e/5S21'
export const titre = 'Calculer des probabilités dans une expérience aléatoire à une épreuve'
export const interactifReady = false

/**
 * @author Sébastien LOZANO
 */
export const uuid = '28dfd'

export const refs = {
  'fr-fr': ['2S30-2'],
  'fr-ch': []
}
export default class FonctionsProbabilite12nde extends FonctionsProbabilite1 {
  constructor () {
    super()
    this.styleCorrection = 'lycee'
  }
}
