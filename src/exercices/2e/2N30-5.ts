import SommeOuProduitFractions from '../4e/4C23'
export const titre = 'Effectuer somme, différence ou produit de fractions'
export const dateDeModifImportante = '07/05/2024'
export const interactifType = 'mathLive'
export const interactifReady = true
export const uuid = '6575c'

export const refs = {
  'fr-fr': ['2N30-5'],
  'fr-ch': ['1CN-5']
}
export default class SommeOuProduitFractions2nde extends SommeOuProduitFractions {
  constructor () {
    super()
    this.sup = 4
  }
}
