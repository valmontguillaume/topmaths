import ÉcrireUneExpressionLitterale from '../5e/5L10'
export const titre = 'Écrire une expression littérale'
export const amcReady = true
export const amcType = 'qcmMult'
export const interactifReady = true
export const interactifType = 'qcm'
export const uuid = 'a16a0'
export const refs = {
  'fr-fr': ['2N40-3'],
  'fr-ch': []
}
export default class EcrireUneExpressionLitterale2e extends ÉcrireUneExpressionLitterale {
}
