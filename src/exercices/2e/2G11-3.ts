import CalculDAngleFigureComplexe from '../3e/3G31-1'
export const titre = 'Résoudre des problèmes utilisant la trigonométrie'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const dateDeModifImportante = '21/05/2024'
export const uuid = '20907'

export const refs = {
  'fr-fr': ['2G11-3'],
  'fr-ch': []
}
export default class CalculDAngleFigureComplexe2nde extends CalculDAngleFigureComplexe {
}
