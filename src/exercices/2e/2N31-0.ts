import PuissanceDunNombre from '../4e/4C35'
export const titre = 'Transformer une écriture de puissance en écriture décimale ou fractionnaire'
export const dateDePublication = '14/06/2022'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '53fbb'
export const refs = {
  'fr-fr': ['2N31-0'],
  'fr-ch': []
}
export default class PuissanceDunNombre2e extends PuissanceDunNombre {
  constructor () {
    super()
    this.sup = true
  }
}
