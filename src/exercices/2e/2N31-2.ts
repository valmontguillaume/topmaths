import PuissancesDunRelatif1 from '../4e/4C33-1'
export const titre = 'Effectuer des calculs avec des puissances'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
export const dateDeModifImportante = '14/09/2023'
export const uuid = '1e42b'
export const refs = {
  'fr-fr': ['2N31-2'],
  'fr-ch': []
}
export default class PuissancesDunRelatif12e extends PuissancesDunRelatif1 {
  constructor () {
    super()
    this.classe = 2
    this.correctionDetaillee = false
    this.sup2 = 3
  }
}
