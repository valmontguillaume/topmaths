import DeterminerDesMedianes from '../4e/4S11'
export const titre = 'Calculer des médianes'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
export const uuid = '4bc38'
export const refs = {
  'fr-fr': ['2S20-2'],
  'fr-ch': []
}
export default class DeterminerDesMedianes2nde extends DeterminerDesMedianes {
}
