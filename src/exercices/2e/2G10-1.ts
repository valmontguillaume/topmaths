import EgaliteDAngles from '../5e/5G30-1'
export const titre = 'Déterminer des angles en utilisant les cas d\'égalité'
export const interactifReady = false
export const amcReady = true
export const amcType = 'AMCHybride'
export const uuid = '430b9'
export const refs = {
  'fr-fr': ['2G10-1'],
  'fr-ch': []
}
export default class EgaliteDAngles2nde extends EgaliteDAngles {
}
