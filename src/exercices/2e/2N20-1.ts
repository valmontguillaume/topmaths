import ListeDesDiviseurs5e from '../5e/5A10'
export const titre = 'Écrire la liste de tous les diviseurs d\'un entier'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDeModifImportante = '28/10/2021'
export const uuid = '7cf48'
export const refs = {
  'fr-fr': ['2N20-1'],
  'fr-ch': []
}
export default class ListeDesDiviseurs2nde extends ListeDesDiviseurs5e {
  constructor () {
    super()
    this.sup = '3-3-3'
    this.sup2 = '8-10-12'
    this.sup3 = 12
  }
}
