import EqResolvantesThales from '../3e/3L13-2'
export const titre = 'Résoudre une équation du type $\\dfrac{x}{a}=\\dfrac{b}{c}$ (v2)'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
export const uuid = '78f02'
export const refs = {
  'fr-fr': ['2N51-3b'],
  'fr-ch': []
}
export default class EqResolvantesThales2nde extends EqResolvantesThales {
}
