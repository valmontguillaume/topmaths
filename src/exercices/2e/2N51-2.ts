import TrouverErreurResolEqDeg1 from '../4e/4L15-0'
export const titre = 'Trouver l\'erreur dans une résolution d\'équation du premier degré'
export const interactifReady = false
export const uuid = 'ad208'
export const refs = {
  'fr-fr': ['2N51-2'],
  'fr-ch': []
}
export default class TrouverErreurResolEqDeg12nde extends TrouverErreurResolEqDeg1 {
}
