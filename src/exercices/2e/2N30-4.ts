import ExerciceDiviserFractions from '../4e/4C22-2'

export const titre = 'Diviser des fractions'
export const amcReady = true
export const amcType = 'AMCNum'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = 'cb572'

export const refs = {
  'fr-fr': ['2N30-4'],
  'fr-ch': []
}
export default class ExerciceDiviserFractions2nde extends ExerciceDiviserFractions {
}
