import GeoTriangle1 from '../geodyn/geoTriangle1'
export const titre = 'Construire un triangle dont on connaît trois longueurs avec un logiciel de géométrie dynamique'
export const interactifReady = true
export const dateDePublication = '1/1/2025'
export const uuid = '79d64'
export const interactifType = 'custom'

export const refs = {
  'fr-fr': ['6G21-6'],
  'fr-ch': []
}
export default class GeoTriangle extends GeoTriangle1 {
}
