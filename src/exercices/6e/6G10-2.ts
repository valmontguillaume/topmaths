import UtiliserLeCodagePourDecrire from './_Utiliser_le_codage_pour_decrire'

export const interactifReady = false
export const titre = 'Utiliser le codage pour décrire ou illustrer une figure'
export const dateDeModifImportante = '17/08/2023'

/**
 * Exercice du test de positionnement 5e
 * Variantes à venir...
 * @author Jean-Claude Lhote
 */
export const uuid = 'e8f0b'

export const refs = {
  'fr-fr': ['6G10-2'],
  'fr-ch': ['9ES1-3']
}
export default class UtiliserLeCodagePourDecrire6e extends UtiliserLeCodagePourDecrire {
  constructor () {
    super()
    this.classe = 6
  }
}
