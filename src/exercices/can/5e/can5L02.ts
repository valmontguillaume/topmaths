import ÉcrireUneExpressionLitterale from '../../5e/5L10'
export const interactifReady = true
export const interactifType = 'qcm'
export const titre = 'Écrire une expression littérale'
export const dateDePublication = '23/11/2023'
export const uuid = 'b7307'
export const refs = {
  'fr-fr': ['can5L02'],
  'fr-ch': []
}
export default class ÉcrireUneExpressionLitteraleCAN extends ÉcrireUneExpressionLitterale {
  constructor () {
    super()
    this.nbQuestions = 1
  }
}
