import ConversionHeuresMinutesOuMinutesEtSecondes from '../../6e/6D13'
export const titre = 'Convertir minutes<->heures ou secondes<->minutes'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'

/**
 * Alias de 6D13.js
 * @author Jean-Claude Lhote
 * Créé pendant l'été 2021

 */
export const uuid = 'd34e5'

export const refs = {
  'fr-fr': ['can6D04'],
  'fr-ch': []
}
export default class ConvertirMinutesHeures extends ConversionHeuresMinutesOuMinutesEtSecondes {
  constructor () {
    super(true)
    this.nbQuestions = 1

    this.correctionDetailleeDisponible = false
  }
}
