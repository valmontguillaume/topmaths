import EcrireEntierSousFormeDeFraction from '../../6e/6N20-0'

export const titre = 'Écrire un nombre entier sous la forme d\'une fraction'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDePublication = '27/11/2024'

/**
 * @author Guillaume Valmont
 */
export const uuid = '6317f'
export const refs = {
  'fr-fr': ['can6N20'],
  'fr-ch': ['']
}
export default class EcrireEntierSousFormeDeFractionCAN extends EcrireEntierSousFormeDeFraction {
}
