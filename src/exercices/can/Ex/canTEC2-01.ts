import { KeyboardType } from '../../../lib/interactif/claviers/keyboard'
import { ecritureAlgebrique, ecritureAlgebriqueSauf1, ecritureParentheseSiNegatif } from '../../../lib/outils/ecritures'
import { miseEnEvidence } from '../../../lib/outils/embellissements'
import { randint } from '../../../modules/outils'
import Exercice from '../../Exercice'
export const titre = 'Calculer un module'
export const interactifReady = true
export const interactifType = 'mathLive'

export const dateDePublication = '05/02/2025'

export const uuid = '28c55'
export const refs = {
  'fr-fr': ['canTEC2-01'],
  'fr-ch': []
}

/**
 * Modèle d'exercice très simple pour la course aux nombres
 * @author Stéphane Guyon

*/
export default class NomExercice extends Exercice {
  constructor () {
    super()
    this.typeExercice = 'simple'
    this.nbQuestions = 1
    this.optionsChampTexte = { texteAvant: '$\\vert z  \\vert =$' }
    this.formatChampTexte = KeyboardType.clavierFullOperations
  }

  nouvelleVersion () {
    const a = randint(-7, 7, 0)
    const b = randint(-7, 7, 0)

    this.question = `Soit $z=${a}${ecritureAlgebriqueSauf1(b)}\\text{i}\\in\\mathbb{C}$. <br>`
    this.correction = 'On sait que si $z=a+\\text{i}b$ alors $\\vert z\\vert=\\sqrt{a^2+b^2}$'
    this.correction += `<br>Il vient ici : $\\vert z\\vert=\\sqrt{${ecritureParentheseSiNegatif(a)}^2+${ecritureParentheseSiNegatif(b)}^2}$,`
    this.correction += ` d'où $\\vert z\\vert=\\sqrt{${a * a}+${b * b}}=\\sqrt{${a * a + b * b}}$.`
    this.correction += `<br>Le module de $z$ est donc $\\vert z\\vert=${miseEnEvidence(`\\sqrt{${a * a + b * b}}`)}$.`
    this.reponse = Math.sqrt(a * a + b * b)
    this.canEnonce = this.question// 'Compléter'
    this.canReponseACompleter = ''
  }
}
