import ConstruireUnTriangle from '../6e/6G21'
export const titre = 'Construire un triangle avec les instruments'
export const interactifReady = false
export const uuid = '36116'
export const refs = {
  'fr-fr': ['5G20-0'],
  'fr-ch': []
}
export default class ConstruireUnTriangle5e extends ConstruireUnTriangle {
  constructor () {
    super()
    this.sup2 = true
  }
}
