import UtiliserLeCodagePourDecrire from '../6e/_Utiliser_le_codage_pour_decrire'
export const titre = 'Utiliser le codage pour décrire ou illustrer une figure'
export const interactifReady = false
export const dateDeModifImportante = '17/08/2023'

export const uuid = '2b6a2'

export const refs = {
  'fr-fr': ['5G30'],
  'fr-ch': ['9ES2-6']
}
export default class UtiliserLeCodagePourDecrire5e extends UtiliserLeCodagePourDecrire {
  constructor () {
    super()
    this.classe = 5
  }
}
