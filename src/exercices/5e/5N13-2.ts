import EgalitesEntreFractions from '../6e/6N41'
export const amcReady = true
export const amcType = 'qcmMono'
export const interactifReady = true
export const interactifType = 'mathLive'
export const titre = 'Égalités entre fractions simples'
export const uuid = '4718e'
export const refs = {
  'fr-fr': ['5N13-2'],
  'fr-ch': ['9NO12-4']
}
export default class EgalitesEntreFractions5e extends EgalitesEntreFractions {
}
