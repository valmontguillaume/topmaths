import TableauCriteresDeDivisibilite from '../6e/6N43-2'
export const titre = 'Utiliser les critères de divisibilité (plusieurs possibles)'
export const interactifReady = true
export const interactifType = 'qcm'
export const uuid = 'a55d2'
export const refs = {
  'fr-fr': ['5A11'],
  'fr-ch': []
}
export default class TableauCriteresDeDivisibilite5e extends TableauCriteresDeDivisibilite {
  constructor () {
    super()
    this.sup = true
  }
}
