import ExerciceLabyrinthePremiers3e from '../3e/3A10-7'
export const titre = 'Parcourir un labyrinthe de nombres premiers'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDePublication = '12/10/2022'
export const dateDeModifImportante = '29/10/2024'
export const uuid = '05079'
export const refs = {
  'fr-fr': ['5A12-3'],
  'fr-ch': []
}
export default class ExerciceLabyrinthePremiers5e extends ExerciceLabyrinthePremiers3e {
  constructor () {
    super()
    this.sup = 1
  }
}
