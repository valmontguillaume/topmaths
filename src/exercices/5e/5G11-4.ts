import TransformationsDuPlanEtCoordonnees from '../3e/3G10-1'
export const titre = 'Trouver les coordonnées de l\'image d\'un point par une symétrie centrale'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const dateDePublication = '28/10/2021'
export const dateDeModifImportante = '28/12/2022'
export const uuid = '08f60'
export const refs = {
  'fr-fr': ['5G11-4'],
  'fr-ch': ['9ES6-9']
}
export default class TransformationsDuPlanEtCoordonnees5e extends TransformationsDuPlanEtCoordonnees {
  constructor () {
    super()
    this.sup = '2'
  }
}
