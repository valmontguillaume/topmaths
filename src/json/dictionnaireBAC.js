/* Thèmes
   Combinatoire
   Convexité
   Équations différentielles
   Espace
   Exponentielle
   Fonctions trigonométriques
   Logarithme
   Loi binomiale
   Primitives
   Probabilités conditionnelles
   Python
   QCM
   Suites
   Variables aléatoires
   Vrai/Faux
   TVI
   Tableur
   Dénombrement
*/
export const dictionnaireBAC = {
  bac_2021_01_sujet0_1: {
    annee: '2021',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Exponentielle', 'Convexité']
  },
  bac_2021_01_sujet0_2: {
    annee: '2021',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_01_sujet0_3: {
    annee: '2021',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Python']
  },
  bac_2021_01_sujet0_4: {
    annee: '2021',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2021_01_sujet0_5: {
    annee: '2021',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Équations différentielles', 'Exponentielle', 'Suites']
  },
  bac_2021_03_sujet1_1: {
    annee: '2021',
    lieu: 'sujet1',
    mois: 'Mars',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_03_sujet1_2: {
    annee: '2021',
    lieu: 'sujet1',
    mois: 'Mars',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle']
  },
  bac_2021_03_sujet1_3: {
    annee: '2021',
    lieu: 'sujet1',
    mois: 'Mars',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['QCM', 'Espace']
  },
  bac_2021_03_sujet1_4: {
    annee: '2021',
    lieu: 'sujet1',
    mois: 'Mars',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Tableur']
  },
  bac_2021_03_sujet1_5: {
    annee: '2021',
    lieu: 'sujet1',
    mois: 'Mars',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2021_03_sujet2_1: {
    annee: '2021',
    lieu: 'sujet2',
    mois: 'Mars',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_03_sujet2_2: {
    annee: '2021',
    lieu: 'sujet2',
    mois: 'Mars',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2021_03_sujet2_3: {
    annee: '2021',
    lieu: 'sujet2',
    mois: 'Mars',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_03_sujet2_4: {
    annee: '2021',
    lieu: 'sujet2',
    mois: 'Mars',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle']
  },
  bac_2021_03_sujet2_5: {
    annee: '2021',
    lieu: 'sujet2',
    mois: 'Mars',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Primitives']
  },
  bac_2021_05_ameriquenord_1: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_05_ameriquenord_2: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2021_05_ameriquenord_3: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_05_ameriquenord_4: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Exponentielle', 'Convexité']
  },
  bac_2021_05_ameriquenord_5: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2021_06_polynesie_1: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Suites']
  },
  bac_2021_06_polynesie_2: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_06_polynesie_3: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_polynesie_4: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité', 'Équations différentielles']
  },
  bac_2021_06_polynesie_5: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI']
  },

  bac_2021_06_asie_1: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2021_06_asie_10: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '10',
    typeExercice: 'bac',
    tags: ['Suites', 'Logarithme', 'Tableur']
  },
  bac_2021_06_asie_2: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_asie_3: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Dénombrement', 'Variables aléatoires', 'Loi binomiale']
  },
  bac_2021_06_asie_4: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Convexité']
  },
  bac_2021_06_asie_5: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Équations différentielles']
  },
  bac_2021_06_asie_6: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Convexité', 'Python']
  },
  bac_2021_06_asie_7: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_asie_8: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'bac',
    tags: ['Variables aléatoires', 'Loi binomiale', 'Dénombrement']
  },
  bac_2021_06_asie_9: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '9',
    typeExercice: 'bac',
    tags: ['Suites', 'Équations différentielles']
  },
  bac_2021_06_etrangers_1: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Dénombrement']
  },
  bac_2021_06_etrangers_10: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '10',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Équations différentielles']
  },
  bac_2021_06_etrangers_2: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_06_etrangers_3: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites']
  },
  bac_2021_06_etrangers_4: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_etrangers_5: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Équations différentielles']
  },
  bac_2021_06_etrangers_6: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Variables aléatoires']
  },
  bac_2021_06_etrangers_7: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'bac',
    tags: ['Suites', 'Exponentielle', 'Python']
  },
  bac_2021_06_etrangers_8: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_etrangers_9: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '9',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Variables aléatoires']
  },
  bac_2021_06_metropole_1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Convexité']
  },
  bac_2021_06_metropole_10: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '10',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité']
  },
  bac_2021_06_metropole_2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_06_metropole_3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2021_06_metropole_4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_06_metropole_5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Équations différentielles', 'Exponentielle']
  },
  bac_2021_06_metropole_6: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'bac',
    tags: ['QCM', 'Espace']
  },
  bac_2021_06_metropole_7: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Python']
  },
  bac_2021_06_metropole_8: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'bac',
    tags: ['Suites']
  },
  bac_2021_06_metropole_9: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '9',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI']
  },
  bac_2021_09_metropole_1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Convexité']
  },
  bac_2021_09_metropole_10: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '10',
    typeExercice: 'bac',
    tags: ['Logarithme']
  },
  bac_2021_09_metropole_2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2021_09_metropole_3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Probabilités conditionnelles']
  },
  bac_2021_09_metropole_4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2021_09_metropole_5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme']
  },
  bac_2021_09_metropole_6: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2021_09_metropole_7: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'bac',
    tags: ['QCM', 'Espace']
  },
  bac_2021_09_metropole_8: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '8',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI']
  },
  bac_2021_09_metropole_9: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '9',
    typeExercice: 'bac',
    tags: ['Suites']
  },

  bac_2022_05_sujet1_ameriquenord_1: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2022_05_sujet1_ameriquenord_2: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Logarithme']
  },
  bac_2022_05_sujet1_ameriquenord_3: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet1_ameriquenord_4: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité', 'Vrai/Faux']
  },
  bac_2022_05_sujet1_asie_1: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet1_asie_2: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Exponentielle', 'Logarithme']
  },
  bac_2022_05_sujet1_asie_3: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet1_asie_4: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2022_05_sujet1_etrangers_1: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Convexité', 'Logarithme', 'Primitives']
  },
  bac_2022_05_sujet1_etrangers_2: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet1_etrangers_3: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Suites']
  },
  bac_2022_05_sujet1_etrangers_4: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet1_madagascar_1: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet1_madagascar_2: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Logarithme', 'Convexité']
  },
  bac_2022_05_sujet1_madagascar_3: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Exponentielle', 'TVI', 'Python']
  },
  bac_2022_05_sujet1_madagascar_4: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet1_metropole_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Suites', 'TVI']
  },
  bac_2022_05_sujet1_metropole_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet1_metropole_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Python']
  },
  bac_2022_05_sujet1_metropole_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'QCM', 'Primitives', 'Convexité']
  },
  bac_2022_05_sujet1_polynesie_1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Logarithme', 'Primitives', 'Suites', 'Convexité', 'Python']
  },
  bac_2022_05_sujet1_polynesie_2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet1_polynesie_3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2022_05_sujet1_polynesie_4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_ameriquenord_1: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Suites']
  },
  bac_2022_05_sujet2_ameriquenord_2: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['TVI', 'Exponentielle', 'Convexité']
  },
  bac_2022_05_sujet2_ameriquenord_3: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_ameriquenord_4: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Loi binomiale', 'QCM']
  },
  bac_2022_05_sujet2_asie_1: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_asie_2: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Convexité', 'Logarithme', 'TVI']
  },
  bac_2022_05_sujet2_asie_3: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2022_05_sujet2_asie_4: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Tableur']
  },
  bac_2022_05_sujet2_etrangers_1: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Convexité', 'Exponentielle', 'Primitives']
  },
  bac_2022_05_sujet2_etrangers_2: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité', 'Suites']
  },
  bac_2022_05_sujet2_etrangers_3: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_etrangers_4: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Variables aléatoires', 'Loi binomiale']
  },
  bac_2022_05_sujet2_madagascar_1: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet2_madagascar_2: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_madagascar_3: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Convexité']
  },
  bac_2022_05_sujet2_madagascar_4: {
    annee: '2022',
    lieu: 'Madagascar',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Logarithme', 'Suites', 'Python']
  },
  bac_2022_05_sujet2_metropole_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet2_metropole_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['QCM', 'Convexité', 'Suites']
  },
  bac_2022_05_sujet2_metropole_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_05_sujet2_metropole_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI']
  },
  bac_2022_05_sujet2_polynesie_1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Logarithme', 'Exponentielle', 'Primitives']
  },
  bac_2022_05_sujet2_polynesie_2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_05_sujet2_polynesie_3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2022_05_sujet2_polynesie_4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_09_sujet1_ameriquesud_1: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_09_sujet1_ameriquesud_2: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Logarithme']
  },
  bac_2022_09_sujet1_ameriquesud_3: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Convexité']
  },
  bac_2022_09_sujet1_ameriquesud_4: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_09_sujet1_metropole_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Convexité', 'Suites', 'Primitives']
  },
  bac_2022_09_sujet1_metropole_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Variables aléatoires', 'Loi binomiale']
  },
  bac_2022_09_sujet1_metropole_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Exponentielle', 'TVI']
  },
  bac_2022_09_sujet1_metropole_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_09_sujet1_polynesie_1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_09_sujet1_polynesie_2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2022_09_sujet1_polynesie_3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité', 'Primitives']
  },
  bac_2022_09_sujet1_polynesie_4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_09_sujet2_ameriquesud_1: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_09_sujet2_ameriquesud_2: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Convexité']
  },
  bac_2022_09_sujet2_ameriquesud_3: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2022_09_sujet2_ameriquesud_4: {
    annee: '2022',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_09_sujet2_metropole_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2022_09_sujet2_metropole_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Python', 'Convexité', 'Primitives']
  },
  bac_2022_09_sujet2_metropole_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Suites']
  },
  bac_2022_09_sujet2_metropole_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_10_sujet1_caledonie_1: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2022_10_sujet1_caledonie_2: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Suites', 'Python']
  },
  bac_2022_10_sujet1_caledonie_3: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_10_sujet1_caledonie_4: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_10_sujet2_caledonie_1: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2022_10_sujet2_caledonie_2: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Python']
  },
  bac_2022_10_sujet2_caledonie_3: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2022_10_sujet2_caledonie_4: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: 'Octobre',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Exponentielle', 'Logarithme']
  },

  bac_2024_01_sujet0_1: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Équations différentielles', 'Exponentielle']
  },
  bac_2024_01_sujet0_2: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Primitives', 'Python', 'Suites', 'Exponentielle']
  },
  bac_2024_01_sujet0_3: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Variables aléatoires', 'Loi binomiale']
  },
  bac_2024_01_sujet0_4: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace', 'QCM']
  },
  bac_2024_01_sujet0_5: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Fonctions trigonométriques', 'Convexité', 'QCM', 'Combinatoire', 'Loi binomiale']
  },
  bac_2024_01_sujet0_6: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '6',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Suites']
  },
  bac_2024_01_sujet0_7: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '7',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Logarithme']
  },
  bac_2024_01_sujet0_8: {
    annee: '2024',
    lieu: 'sujet0',
    mois: 'Janvier',
    numeroInitial: '8',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },

  bac_2023_03_sujet1_ameriquenord_1: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet1_ameriquenord_2: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI', 'Convexité']
  },
  bac_2023_03_sujet1_ameriquenord_3: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_ameriquenord_4: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet1_asie_1: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet1_asie_2: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_asie_3: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme']
  },
  bac_2023_03_sujet1_asie_4: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles']
  },
  bac_2023_03_sujet1_etrangers_groupe1_1: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Logarithme', 'Primitives', 'Python', 'Loi binomiale']
  },
  bac_2023_03_sujet1_etrangers_groupe1_2: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Suites', 'Loi binomiale']
  },
  bac_2023_03_sujet1_etrangers_groupe1_3: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_etrangers_groupe1_4: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Exponentielle']
  },
  bac_2023_03_sujet1_etrangers_groupe2_1: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Logarithme']
  },
  bac_2023_03_sujet1_etrangers_groupe2_2: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Suites']
  },
  bac_2023_03_sujet1_etrangers_groupe2_3: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['QCM', 'Logarithme', 'Convexité', 'Suites']
  },
  bac_2023_03_sujet1_etrangers_groupe2_4: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet1_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI']
  },
  bac_2023_03_sujet1_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet1_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_polynesie_1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet1_polynesie_2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet1_polynesie_3: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Convexité', 'Primitives', 'Exponentielle', 'Python']
  },
  bac_2023_03_sujet1_polynesie_4: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Logarithme']
  },
  bac_2023_03_sujet1_reunion_1: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet1_reunion_2: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Convexité', 'Primitives']
  },
  bac_2023_03_sujet1_reunion_3: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['QCM', 'Suites', 'Python']
  },
  bac_2023_03_sujet1_reunion_4: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_ameriquenord_1: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Convexité', 'Exponentielle']
  },
  bac_2023_03_sujet2_ameriquenord_2: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet2_ameriquenord_3: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_ameriquenord_4: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Logarithme', 'TVI', 'Suites', 'Variables aléatoires', 'Loi binomiale']
  },
  bac_2023_03_sujet2_asie_1: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_asie_2: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI', 'Logarithme']
  },
  bac_2023_03_sujet2_asie_3: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet2_asie_4: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles']
  },
  bac_2023_03_sujet2_etrangers_groupe1_1: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Primitives', 'Convexité', 'Python', 'Probabilités conditionnelles']
  },
  bac_2023_03_sujet2_etrangers_groupe1_2: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Suites']
  },
  bac_2023_03_sujet2_etrangers_groupe1_3: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_etrangers_groupe1_4: {
    annee: '2023',
    lieu: 'Centres étrangers G1',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Loi binomiale']
  },
  bac_2023_03_sujet2_etrangers_groupe2_1: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité']
  },
  bac_2023_03_sujet2_etrangers_groupe2_2: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Suites']
  },
  bac_2023_03_sujet2_etrangers_groupe2_3: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_etrangers_groupe2_4: {
    annee: '2023',
    lieu: 'Centres étrangers G2',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Loi binomiale', 'Python']
  },
  bac_2023_03_sujet2_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet2_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet2_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme']
  },
  bac_2023_03_sujet2_polynesie_1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Suites', 'Loi binomiale']
  },
  bac_2023_03_sujet2_polynesie_2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_polynesie_3: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité']
  },
  bac_2023_03_sujet2_polynesie_4: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Suites', 'Logarithme', 'Convexité', 'Python']
  },
  bac_2023_03_sujet2_reunion_1: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_03_sujet2_reunion_2: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_03_sujet2_reunion_3: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_03_sujet2_reunion_4: {
    annee: '2023',
    lieu: 'reunion',
    mois: 'Mars',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Convexité', 'Exponentielle', 'Logarithme']
  },
  bac_2023_08_sujet1_caledonie_1: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_08_sujet1_caledonie_2: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_08_sujet1_caledonie_3: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['QCM', 'Primitives', 'Espace', 'Exponentielle']
  },
  bac_2023_08_sujet1_caledonie_4: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité', 'TVI']
  },
  bac_2023_08_sujet2_caledonie_1: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_08_sujet2_caledonie_2: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI', 'Convexité']
  },
  bac_2023_08_sujet2_caledonie_3: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_08_sujet2_caledonie_4: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Août',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles']
  },
  bac_2023_09_sujet1_ameriquesud_1: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Python']
  },
  bac_2023_09_sujet1_ameriquesud_2: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_09_sujet1_ameriquesud_3: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_09_sujet1_ameriquesud_4: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites']
  },
  bac_2023_09_sujet1_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['QCM', 'Exponentielle', 'Suites', 'Primitives', 'Python']
  },
  bac_2023_09_sujet1_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_09_sujet1_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_09_sujet1_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Convexité', 'Exponentielle']
  },
  bac_2023_09_sujet1_polynesie_1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_09_sujet1_polynesie_2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI', 'Convexité']
  },
  bac_2023_09_sujet1_polynesie_3: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_09_sujet1_polynesie_4: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_09_sujet2_ameriquesud_1: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Variables aléatoires', 'Loi binomiale']
  },
  bac_2023_09_sujet2_ameriquesud_2: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2023_09_sujet2_ameriquesud_3: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2023_09_sujet2_ameriquesud_4: {
    annee: '2023',
    lieu: 'Amérique du sud',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Exponentielle', 'TVI', 'Convexité']
  },
  bac_2023_09_sujet2_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2023_09_sujet2_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité']
  },
  bac_2023_09_sujet2_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Exponentielle']
  },
  bac_2023_09_sujet2_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['QCM', 'Espace']
  },

  bac_2024_05_sujet1_ameriquenord_1: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2024_05_sujet1_ameriquenord_2: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_05_sujet1_ameriquenord_3: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI']
  },
  bac_2024_05_sujet1_ameriquenord_4: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Primitives', 'Suites', 'Python']
  },
  bac_2024_05_sujet2_ameriquenord_1: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2024_05_sujet2_ameriquenord_2: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_05_sujet2_ameriquenord_3: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2024_05_sujet2_ameriquenord_4: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Primitives']
  },
  bac_2024_06_sujet1_etrangers_1: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles']
  },
  bac_2024_06_sujet1_etrangers_2: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Suites', 'Python']
  },
  bac_2024_06_sujet1_etrangers_3: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Équations différentielles', 'Fonctions trigonométriques']
  },
  bac_2024_06_sujet1_etrangers_4: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet2_etrangers_1: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_06_sujet2_etrangers_2: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Convexité', 'TVI']
  },
  bac_2024_06_sujet2_etrangers_3: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet2_etrangers_4: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },

  bac_2024_06_sujet1_asie_1: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Convexité', 'Exponentielle', 'Primitives']
  },
  bac_2024_06_sujet1_asie_2: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet1_asie_3: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Loi binomiale', 'Probabilités conditionnelles']
  },
  bac_2024_06_sujet1_asie_4: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Suites', 'Python']
  },
  bac_2024_06_sujet1_metropole_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Exponentielle', 'Équations différentielles', 'Suites']
  },
  bac_2024_06_sujet1_metropole_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_06_sujet1_metropole_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet1_metropole_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Logarithme', 'TVI', 'Intégration']
  },
  bac_2024_06_sujet1_metropole_devoile_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet1_metropole_devoile_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_06_sujet1_metropole_devoile_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Suites', 'Python', 'Intégration']
  },
  bac_2024_06_sujet1_metropole_devoile_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Exponentielle', 'Convexité', 'Logarithme', 'TVI']
  },
  bac_2024_06_sujet1_polynesie_1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Espace']
  },
  bac_2024_06_sujet1_polynesie_2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Équations différentielles', 'Intégration']
  },
  bac_2024_06_sujet1_polynesie_3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Loi binomiale', 'Probabilités conditionnelles']
  },
  bac_2024_06_sujet1_polynesie_4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2024_06_sujet2_asie_1: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Convexité', 'Suites']
  },
  bac_2024_06_sujet2_asie_2: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Suites', 'Python']
  },
  bac_2024_06_sujet2_asie_3: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Convexité', 'Dénombrement', 'Logarithme', 'Équations différentielles']
  },
  bac_2024_06_sujet2_asie_4: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_06_sujet2_metropole_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_06_sujet2_metropole_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Équations différentielles']
  },
  bac_2024_06_sujet2_metropole_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Convexité', 'Logarithme', 'TVI']
  },
  bac_2024_06_sujet2_metropole_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Espace']
  },
  bac_2024_06_sujet2_metropole_devoile_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_06_sujet2_metropole_devoile_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Espace']
  },
  bac_2024_06_sujet2_metropole_devoile_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Suites', 'Python', 'Logarithme']
  },
  bac_2024_06_sujet2_metropole_devoile_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: 'devoile',
    typeExercice: 'bac',
    tags: ['Convexité', 'Primitives', 'Exponentielle', 'Intégration']
  },
  bac_2024_06_sujet2_polynesie_1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2024_06_sujet2_polynesie_2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['QCM', 'Équations différentielles', 'Intégration', 'Logarithme', 'Dénombrement']
  },
  bac_2024_06_sujet2_polynesie_3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Logarithme', 'Python']
  },
  bac_2024_06_sujet2_polynesie_4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    jour: 'J2',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_05_sujet2_ameriquenord_5: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    jour: 'J2',
    numeroInitial: '5',
    typeExercice: 'bac',
    tags: ['Logarithme', 'Primitives', 'Intégration']
  },
  bac_2024_09_sujet1_metropole_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Espace']
  },
  bac_2024_09_sujet1_metropole_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'Intégration']
  },
  bac_2024_09_sujet1_metropole_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Suites', 'Python']
  },
  bac_2024_09_sujet1_metropole_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Loi binomiale', 'Variables aléatoires']
  },
  bac_2024_09_sujet1_polynesie_1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale']
  },
  bac_2024_09_sujet1_polynesie_2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Exponentielle', 'Logarithme', 'TVI', 'Équations différentielles', 'Intégration']
  },
  bac_2024_09_sujet1_polynesie_3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Suites', 'Python']
  },
  bac_2024_09_sujet1_polynesie_4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },

  bac_2024_06_sujet1_suede_1: {
    annee: '2024',
    lieu: 'suede',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '1',
    typeExercice: 'bac',
    tags: ['Vrai/Faux', 'Équations différentielles', 'Suites', 'Python', 'Logarithme']
  },
  bac_2024_06_sujet1_suede_2: {
    annee: '2024',
    lieu: 'suede',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '2',
    typeExercice: 'bac',
    tags: ['Probabilités conditionnelles', 'Loi binomiale', 'Suites']
  },
  bac_2024_06_sujet1_suede_3: {
    annee: '2024',
    lieu: 'suede',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '3',
    typeExercice: 'bac',
    tags: ['Exponentielle', 'TVI', 'Intégration']
  },
  bac_2024_06_sujet1_suede_4: {
    annee: '2024',
    lieu: 'suede',
    mois: 'Juin',
    jour: 'J1',
    numeroInitial: '4',
    typeExercice: 'bac',
    tags: ['Espace']
  },

}
