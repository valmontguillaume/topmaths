/*
  Vrai-faux
  Agrandissement-réduction
  Aires et périmètres
  Algorithmique-programmation
  Arithmétique
  Calcul littéral
  Calcul numérique
  Coordonnées terrestres
  Durées
  Équations
  Fonctions
  Fractions
  Géométrie dans l'espace
  Géométrie plane
  Grandeurs composées
  Lecture graphique
  Recherche d\'informations
  Statistiques
  Pourcentages
  Prise d\'initiatives
  Probabilités
  Programme de calculs
  Proportionnalité
  Puissances
  Pythagore
  QCM
  Tableur
  Thalès
  Transformations
  Triangles semblables
  Trigonométrie
  Volumes
  Vitesses
*/

export const dictionnaireDNB = {
  dnb_2013_04_pondichery_1: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Calcul numérique', 'Arithmétique', "Géométrie dans l'espace", 'Thalès']
  },
  dnb_2013_04_pondichery_2: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2013_04_pondichery_3: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Trigonométrie', 'Pourcentages']
  },
  dnb_2013_04_pondichery_4: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Calcul littéral', 'Équations']
  },
  dnb_2013_04_pondichery_5: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Aires et périmètres', 'Agrandissement-réduction', 'Pythagore']
  },
  dnb_2013_04_pondichery_6: {
    annee: '2013',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Proportionnalité', 'Durées', 'Vitesses', 'Puissances']
  },
  dnb_2013_06_ameriquenord_1: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Probabilités', 'Pourcentages', 'Aires et périmètres']
  },
  dnb_2013_06_ameriquenord_2: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Hors programme']
  },
  dnb_2013_06_ameriquenord_3: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Pourcentages']
  },
  dnb_2013_06_ameriquenord_4: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2013_06_ameriquenord_5: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral']
  },
  dnb_2013_06_ameriquenord_6: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fonctions', 'Volumes']
  },
  dnb_2013_06_ameriquenord_7: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Aires et périmètres', 'Pythagore', 'Trigonométrie']
  },
  dnb_2013_06_ameriquenord_8: {
    annee: '2013',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres']
  },
  dnb_2013_06_asie_1: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2013_06_asie_2: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Arithmétique', 'Calcul numérique', 'Calcul littéral']
  },
  dnb_2013_06_asie_3: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Statistiques']
  },
  dnb_2013_06_asie_4: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fonctions', 'Équations', 'Programme de calculs']
  },
  dnb_2013_06_asie_5: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Proportionnalité']
  },
  dnb_2013_06_asie_6: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pourcentages']
  },
  dnb_2013_06_asie_7: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction']
  },
  dnb_2013_06_asie_8: {
    annee: '2013',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Thalès', 'Pythagore', 'Aires et périmètres']
  },
  dnb_2013_06_etrangers_1: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul littéral', 'Équations', 'Volumes', "Géométrie dans l'espace"]
  },
  dnb_2013_06_etrangers_2: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Statistiques']
  },
  dnb_2013_06_etrangers_3: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Géométrie plane']
  },
  dnb_2013_06_etrangers_4: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité']
  },
  dnb_2013_06_etrangers_5: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2013_06_etrangers_6: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Thalès', 'Calcul littéral']
  },
  dnb_2013_06_etrangers_7: {
    annee: '2013',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', "Recherche d'informations"]
  },
  dnb_2013_06_metropole_1: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2013_06_metropole_2: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Fonctions']
  },
  dnb_2013_06_metropole_3: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2013_06_metropole_4: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Trigonométrie']
  },
  dnb_2013_06_metropole_5: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", "Recherche d'informations", 'Volumes', 'Proportionnalité']
  },
  dnb_2013_06_metropole_6: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Volumes']
  },
  dnb_2013_06_metropole_7: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Calcul numérique', 'Pourcentages', 'Calcul littéral']
  },
  dnb_2013_06_polynesie_1: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Proportionnalité', 'Vitesses', 'Durées', 'Agrandissement-réduction', 'Calcul littéral']
  },
  dnb_2013_06_polynesie_2: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2013_06_polynesie_3: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pourcentages']
  },
  dnb_2013_06_polynesie_4: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Géométrie plane']
  },
  dnb_2013_06_polynesie_5: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2013_06_polynesie_6: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Aires et périmètres']
  },
  dnb_2013_06_polynesie_7: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Thalès']
  },
  dnb_2013_06_polynesie_8: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Tableur', 'Statistiques']
  },
  dnb_2013_09_metropole_1: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2013_09_metropole_2: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Probabilités']
  },
  dnb_2013_09_metropole_3: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Calcul littéral', 'Équations']
  },
  dnb_2013_09_metropole_4: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Pourcentages']
  },
  dnb_2013_09_metropole_5: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Aires et périmètres']
  },
  dnb_2013_09_metropole_6: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore', 'Trigonométrie']
  },
  dnb_2013_09_metropole_7: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Vitesses', 'Calcul littéral', 'Statistiques']
  },
  dnb_2013_09_metropole_8: {
    annee: '2013',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Volumes']
  },
  dnb_2013_09_polynesie_1: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur']
  },
  dnb_2013_09_polynesie_2: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Volumes']
  },
  dnb_2013_09_polynesie_3: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Probabilités']
  },
  dnb_2013_09_polynesie_4: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres', 'Durées', 'Proportionnalité', 'Vitesses']
  },
  dnb_2013_09_polynesie_5: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Thalès', 'Calcul littéral']
  },
  dnb_2013_09_polynesie_6: {
    annee: '2013',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Proportionnalité']
  },
  dnb_2013_11_ameriquesud_1: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques', "Recherche d'informations", 'Pourcentages']
  },
  dnb_2013_11_ameriquesud_2: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Pythagore', 'Proportionnalité']
  },
  dnb_2013_11_ameriquesud_3: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Hors programme']
  },
  dnb_2013_11_ameriquesud_4: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Proportionnalité', 'Grandeurs composées']
  },
  dnb_2013_11_ameriquesud_5: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Géométrie plane']
  },
  dnb_2013_11_ameriquesud_6: {
    annee: '2013',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Calcul littéral', 'Fonctions']
  }, // amerique sud secours nov 2013
  dnb_2013_11_amdusudsecours_1: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Calcul littéral', 'Statistiques', 'Hors programme']
  },
  dnb_2013_11_amdusudsecours_2: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Calcul littéral', 'Équations']
  },
  dnb_2013_11_amdusudsecours_3: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Vitesses']
  },
  dnb_2013_11_amdusudsecours_4: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes']
  },
  dnb_2013_11_amdusudsecours_5: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Tableur', 'Fonctions']
  },
  dnb_2013_11_amdusudsecours_6: {
    annee: '2013',
    lieu: 'Amérique du sud (Secours)',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives"]
  }, // fin amerique sud secours nov 2013
  dnb_2013_12_caledonie_1: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Calcul numérique']
  },
  dnb_2013_12_caledonie_2: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Équations']
  },
  dnb_2013_12_caledonie_3: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Aires et périmètres']
  },
  dnb_2013_12_caledonie_4: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Géométrie plane']
  },
  dnb_2013_12_caledonie_5: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Thalès']
  },
  dnb_2013_12_caledonie_6: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Équations']
  },
  dnb_2013_12_caledonie_7: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2013_12_caledonie_8: {
    annee: '2013',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Fonctions']
  }, // Calédonie mars 2014
  dnb_2014_03_caledonie_1: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Thalès', 'Calcul numérique', 'Fonctions', 'Lecture graphique']
  },
  dnb_2014_03_caledonie_2: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres']
  },
  dnb_2014_03_caledonie_3: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Hors programme', 'Arithmétique', 'Probabilités']
  },
  dnb_2014_03_caledonie_4: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Calcul littéral']
  },
  dnb_2014_03_caledonie_5: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2014_03_caledonie_6: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Hors programme', 'Géométrie plane', 'Pythagore']
  },
  dnb_2014_03_caledonie_7: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux']
  },
  dnb_2014_03_caledonie_8: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['QCM', 'Tableur']
  }, // Fin Calédonie mars 2014
  dnb_2014_04_pondichery_1: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2014_04_pondichery_2: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Aires et périmètres', 'Fonctions', 'Probabilités', 'Calcul littéral']
  },
  dnb_2014_04_pondichery_3: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Programme de calculs']
  },
  dnb_2014_04_pondichery_4: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie']
  },
  dnb_2014_04_pondichery_5: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction', 'Fonctions']
  },
  dnb_2014_04_pondichery_6: {
    annee: '2014',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Pourcentages']
  },
  dnb_2014_06_ameriquenord_1: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Arithmétique', 'Équations']
  },
  dnb_2014_06_ameriquenord_2: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Volumes']
  },
  dnb_2014_06_ameriquenord_3: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Vitesses', 'Volumes', 'Pourcentages']
  },
  dnb_2014_06_ameriquenord_4: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Tableur']
  },
  dnb_2014_06_ameriquenord_5: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Géométrie dans l'espace", 'Pythagore']
  },
  dnb_2014_06_ameriquenord_6: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2014_06_ameriquenord_7: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Équations']
  },
  dnb_2014_06_ameriquenord_8: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Aires et périmètres', 'Proportionnalité', 'Durées']
  },
  dnb_2014_06_ameriquenord_9: {
    annee: '2014',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '9',
    typeExercice: 'dnb',
    tags: ['Trigonométrie']
  },
  dnb_2014_06_asie_1: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Puissances']
  },
  dnb_2014_06_asie_2: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2014_06_asie_3: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Agrandissement-réduction']
  },
  dnb_2014_06_asie_4: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Pourcentages', 'Arithmétique', 'Programme de calculs', 'Calcul littéral']
  },
  dnb_2014_06_asie_5: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Géométrie plane']
  },
  dnb_2014_06_asie_6: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Probabilités']
  },
  dnb_2014_06_asie_7: {
    annee: '2014',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore', 'Trigonométrie']
  },
  dnb_2014_06_etrangers_1: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Arithmétique']
  },
  dnb_2014_06_etrangers_2: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore']
  },
  dnb_2014_06_etrangers_3: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Géométrie plane', 'Trigonométrie']
  },
  dnb_2014_06_etrangers_4: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction', 'Proportionnalité']
  },
  dnb_2014_06_etrangers_5: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral']
  },
  dnb_2014_06_etrangers_6: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Proportionnalité']
  },
  dnb_2014_06_etrangers_7: {
    annee: '2014',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2014_06_metropole_1: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Géométrie plane']
  },
  dnb_2014_06_metropole_2: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ["Recherche d'informations", 'Pourcentages']
  },
  dnb_2014_06_metropole_3: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2014_06_metropole_4: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Probabilités']
  },
  dnb_2014_06_metropole_5: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['QCM', 'Agrandissement-réduction', 'Vitesses', 'Calcul numérique', 'Puissances']
  },
  dnb_2014_06_metropole_6: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Trigonométrie']
  },
  dnb_2014_06_metropole_7: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité', 'Pythagore']
  },
  dnb_2014_06_polynesie_1: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2014_06_polynesie_2: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2014_06_polynesie_3: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur']
  },
  dnb_2014_06_polynesie_4: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Arithmétique', 'Calcul numérique']
  },
  dnb_2014_06_polynesie_5: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Tableur']
  },
  dnb_2014_06_polynesie_6: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité', "Recherche d'informations", 'Durées']
  },
  dnb_2014_06_polynesie_7: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Géométrie plane']
  },
  dnb_2014_09_metropole_1: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Vitesses']
  },
  dnb_2014_09_metropole_2: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Volumes', 'Géométrie plane', 'Pythagore', 'Fonctions']
  },
  dnb_2014_09_metropole_3: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Pourcentages']
  },
  dnb_2014_09_metropole_4: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie']
  },
  dnb_2014_09_metropole_5: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Tableur', 'Équations']
  },
  dnb_2014_09_metropole_6: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Agrandissement-réduction', 'Aires et périmètres']
  },
  dnb_2014_09_metropole_7: {
    annee: '2014',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Recherche d'informations", 'Durées', 'Proportionnalité']
  },
  dnb_2014_09_polynesie_1: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul numérique']
  },
  dnb_2014_09_polynesie_2: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Pythagore']
  },
  dnb_2014_09_polynesie_3: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2014_09_polynesie_4: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Thalès', 'Aires et périmètres']
  },
  dnb_2014_09_polynesie_5: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2014_09_polynesie_6: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres', 'Équations']
  },
  dnb_2014_09_polynesie_7: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Tableur', 'Calcul littéral', 'Équations']
  },
  dnb_2014_09_polynesie_8: {
    annee: '2014',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ["Recherche d'informations", 'Statistiques', 'Pourcentages', 'Équations']
  },
  dnb_2014_11_ameriquesud_1: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Vitesses']
  },
  dnb_2014_11_ameriquesud_2: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', "Géométrie dans l'espace", 'Volumes']
  },
  dnb_2014_11_ameriquesud_3: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ["Recherche d'informations"]
  },
  dnb_2014_11_ameriquesud_4: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2014_11_ameriquesud_5: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Durées', 'Tableur', 'Fonctions']
  },
  dnb_2014_11_ameriquesud_6: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Pourcentages']
  },
  dnb_2014_11_ameriquesud_7: {
    annee: '2014',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Thalès']
  },
  dnb_2014_12_caledonie_1: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Pourcentages']
  },
  dnb_2014_12_caledonie_2: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2014_12_caledonie_3: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Thalès']
  },
  dnb_2014_12_caledonie_4: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Vitesses', "Recherche d'informations"]
  },
  dnb_2014_12_caledonie_5: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2014_12_caledonie_6: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pythagore']
  },
  dnb_2014_12_caledonie_7: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Équations']
  },
  dnb_2014_12_caledonie_8: {
    annee: '2014',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité']
  },
  dnb_2015_04_pondichery_1: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul littéral', 'Équations', 'Fonctions', 'Agrandissement-réduction', 'Tableur']
  },
  dnb_2015_04_pondichery_2: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2015_04_pondichery_3: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Calcul numérique', 'Pourcentages']
  },
  dnb_2015_04_pondichery_4: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction']
  },
  dnb_2015_04_pondichery_5: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_04_pondichery_6: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Aires et périmètres']
  },
  dnb_2015_04_pondichery_7: {
    annee: '2015',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Aires et périmètres', 'Calcul littéral']
  },
  dnb_2015_06_ameriquenord_1: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Calcul littéral', 'Pourcentages', 'Agrandissement-réduction']
  },
  dnb_2015_06_ameriquenord_2: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité']
  },
  dnb_2015_06_ameriquenord_3: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Probabilités']
  },
  dnb_2015_06_ameriquenord_4: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Programme de calculs']
  },
  dnb_2015_06_ameriquenord_5: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Thalès']
  },
  dnb_2015_06_ameriquenord_6: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Durées']
  },
  dnb_2015_06_asie_1: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Calcul littéral', 'Calcul numérique']
  },
  dnb_2015_06_asie_2: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Proportionnalité']
  },
  dnb_2015_06_asie_3: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_06_asie_4: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Arithmétique']
  },
  dnb_2015_06_asie_5: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Proportionnalité']
  },
  dnb_2015_06_asie_6: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Équations']
  },
  dnb_2015_06_asie_7: {
    annee: '2015',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes']
  },
  dnb_2015_06_etrangers_1: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_06_etrangers_2: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Vitesses']
  },
  dnb_2015_06_etrangers_3: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Géométrie plane', 'Pythagore']
  },
  dnb_2015_06_etrangers_4: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Tableur', 'Programme de calculs']
  },
  dnb_2015_06_etrangers_5: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité', 'Équations']
  },
  dnb_2015_06_etrangers_6: {
    annee: '2015',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Grandeurs composées']
  }, // Maroc
  dnb_2015_06_etrangers_maroc_1: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2015_06_etrangers_maroc_2: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Équations', 'Puissances', 'Calcul numérique']
  },
  dnb_2015_06_etrangers_maroc_3: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_06_etrangers_maroc_4: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Tableur', 'Statistiques']
  },
  dnb_2015_06_etrangers_maroc_5: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Pourcentages', 'Fonctions', 'Thalès']
  },
  dnb_2015_06_etrangers_maroc_6: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Programme de calculs']
  },
  dnb_2015_06_etrangers_maroc_7: {
    annee: '2015',
    lieu: 'Centres étrangers - Maroc',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Aires et périmètres', 'Grandeurs composées']
  },
  dnb_2015_06_metropole_1: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Pourcentages']
  },
  dnb_2015_06_metropole_2: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Programme de calculs']
  },
  dnb_2015_06_metropole_3: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2015_06_metropole_4: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Probabilités', 'Puissances', 'Arithmétique', 'Équations']
  },
  dnb_2015_06_metropole_5: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Aires et périmètres', 'Calcul numérique', 'Proportionnalité']
  },
  dnb_2015_06_metropole_6: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Fonctions', 'Vitesses']
  },
  dnb_2015_06_metropole_7: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Calcul numérique']
  },
  dnb_2015_06_polynesie_1: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_06_polynesie_2: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2015_06_polynesie_3: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Thalès']
  },
  dnb_2015_06_polynesie_4: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Puissances', 'Calcul littéral']
  },
  dnb_2015_06_polynesie_5: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Vitesses']
  },
  dnb_2015_06_polynesie_6: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Tableur', 'Équations', 'Programme de calculs']
  },
  dnb_2015_06_polynesie_7: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Proportionnalité', 'Aires et périmètres']
  },
  dnb_2015_09_metropole_1: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Équations', 'Programme de calculs']
  },
  dnb_2015_09_metropole_2: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Aires et périmètres']
  },
  dnb_2015_09_metropole_3: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Proportionnalité', 'Durées', 'Vitesses']
  },
  dnb_2015_09_metropole_4: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Équations']
  },
  dnb_2015_09_metropole_5: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Programme de calculs']
  },
  dnb_2015_09_metropole_6: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Durées', 'Pourcentages', 'Probabilités', 'Statistiques']
  },
  dnb_2015_09_metropole_7: {
    annee: '2015',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore', 'Trigonométrie']
  },
  dnb_2015_09_polynesie_1: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations']
  },
  dnb_2015_09_polynesie_2: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Trigonométrie', 'Équations', 'Pourcentages', 'Probabilités']
  },
  dnb_2015_09_polynesie_3: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Calcul numérique']
  },
  dnb_2015_09_polynesie_4: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Hors programme']
  },
  dnb_2015_09_polynesie_5: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Proportionnalité', 'Volumes']
  },
  dnb_2015_09_polynesie_6: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2015_09_polynesie_7: {
    annee: '2015',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Grandeurs composées', 'Tableur', 'Calcul numérique']
  },
  dnb_2015_12_ameriquesud_1: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Arithmétique', 'Hors programme']
  },
  dnb_2015_12_ameriquesud_2: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur']
  },
  dnb_2015_12_ameriquesud_3: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_12_ameriquesud_4: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie']
  },
  dnb_2015_12_ameriquesud_5: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Calcul littéral', 'Proportionnalité']
  },
  dnb_2015_12_ameriquesud_6: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Volumes', 'Pourcentages', 'Proportionnalité']
  },
  dnb_2015_12_ameriquesud_7: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction', 'Pythagore']
  },
  dnb_2015_12_ameriquesud_8: {
    annee: '2015',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Équations']
  },
  dnb_2015_12_caledonie_1: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Aires et périmètres', 'Probabilités', 'Volumes', 'Équations']
  },
  dnb_2015_12_caledonie_2: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Trigonométrie']
  },
  dnb_2015_12_caledonie_3: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pourcentages']
  },
  dnb_2015_12_caledonie_4: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore']
  },
  dnb_2015_12_caledonie_5: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_12_caledonie_6: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Aires et périmètres']
  },
  dnb_2015_12_caledonie_7: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Hors programme']
  },
  dnb_2015_12_caledonie_8: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2015_12_caledonie_9: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '9',
    typeExercice: 'dnb',
    tags: ['Thalès']
  },
  dnb_2016_04_pondichery_1: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Durées', 'Vitesses']
  },
  dnb_2016_04_pondichery_2: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Statistiques', 'Tableur']
  },
  dnb_2016_04_pondichery_3: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Probabilités']
  },
  dnb_2016_04_pondichery_4: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Pythagore', 'Thalès']
  },
  dnb_2016_04_pondichery_5: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fonctions']
  },
  dnb_2016_04_pondichery_6: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pourcentages']
  },
  dnb_2016_04_pondichery_7: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Hors programme', 'QCM']
  },
  dnb_2016_04_pondichery_8: {
    annee: '2016',
    lieu: 'Pondichéry',
    mois: 'Avril',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Aires et périmètres', 'Volumes']
  },
  dnb_2016_06_ameriquenord_1: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Équations', 'Pourcentages', 'Pythagore']
  },
  dnb_2016_06_ameriquenord_2: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2016_06_ameriquenord_3: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur']
  },
  dnb_2016_06_ameriquenord_4: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Trigonométrie', 'Durées']
  },
  dnb_2016_06_ameriquenord_5: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité']
  },
  dnb_2016_06_ameriquenord_6: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Volumes']
  },
  dnb_2016_06_ameriquenord_7: {
    annee: '2016',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions', "Recherche d'informations"]
  },
  dnb_2016_06_metropole_1: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Probabilités']
  },
  dnb_2016_06_metropole_2: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs']
  },
  dnb_2016_06_metropole_3: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Géométrie plane', "Prise d'initiatives"]
  },
  dnb_2016_06_metropole_4: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pourcentages']
  },
  dnb_2016_06_metropole_5: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Aires et périmètres', 'Thalès']
  },
  dnb_2016_06_metropole_6: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Aires et périmètres']
  },
  dnb_2016_06_metropole_7: {
    annee: '2016',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Volumes']
  },
  dnb_2016_06_asie_1: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Équations', 'Probabilités', 'Agrandissement-réduction', 'Hors programme']
  },
  dnb_2016_06_asie_2: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Thalès']
  },
  dnb_2016_06_asie_3: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2016_06_asie_4: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Aires et périmètres', 'Vitesses', "Recherche d'informations"]
  },
  dnb_2016_06_asie_5: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur']
  },
  dnb_2016_06_asie_6: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Calcul numérique', 'Pourcentages']
  },
  dnb_2016_06_asie_7: {
    annee: '2016',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Proportionnalité', 'Volumes']
  },
  dnb_2016_06_etrangers_1: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Fonctions', 'Trigonométrie', 'QCM']
  },
  dnb_2016_06_etrangers_2: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Pourcentages', 'Proportionnalité', 'Vrai-faux', 'Vitesses']
  },
  dnb_2016_06_etrangers_3: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur']
  },
  dnb_2016_06_etrangers_4: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes']
  },
  dnb_2016_06_etrangers_5: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Équations']
  },
  dnb_2016_06_etrangers_6: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2016_06_etrangers_7: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Volumes']
  },
  dnb_2016_06_etrangers_8: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité', 'Volumes']
  },
  dnb_2016_06_etrangers_9: {
    annee: '2016',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '9',
    typeExercice: 'dnb',
    tags: ['Pourcentages', "Recherche d'informations"]
  },
  dnb_2016_06_polynesie_1: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2016_06_polynesie_2: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs']
  },
  dnb_2016_06_polynesie_3: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Thalès', 'Hors programme', 'Géométrie plane']
  },
  dnb_2016_06_polynesie_4: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Statistiques', 'Tableur', 'Durées', 'Vitesses', "Recherche d'informations"]
  },
  dnb_2016_06_polynesie_5: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', "Géométrie dans l'espace"]
  },
  dnb_2016_06_polynesie_6: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité', "Recherche d'informations"]
  },
  dnb_2016_06_polynesie_7: {
    annee: '2016',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Grandeurs composées']
  },
  dnb_2016_09_metropole_1: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Grandeurs composées', 'Durées']
  },
  dnb_2016_09_metropole_2: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2016_09_metropole_3: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Proportionnalité', 'Tableur']
  },
  dnb_2016_09_metropole_4: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fonctions', 'Fractions', 'Hors programme', 'Vitesses']
  },
  dnb_2016_09_metropole_5: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Hors programme', 'Géométrie plane', "Prise d'initiatives"]
  },
  dnb_2016_09_metropole_6: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Pourcentages', 'Proportionnalité', 'Trigonométrie', 'Aires et périmètres']
  },
  dnb_2016_09_metropole_7: {
    annee: '2016',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Équations', 'Aires et périmètres']
  },
  dnb_2016_12_ameriquesud_1: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Probabilités', 'Puissances', 'QCM']
  },
  dnb_2016_12_ameriquesud_2: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Fractions', 'Vitesses', "Recherche d'informations"]
  },
  dnb_2016_12_ameriquesud_3: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2016_12_ameriquesud_4: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Thalès']
  },
  dnb_2016_12_ameriquesud_5: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', "Recherche d'informations"]
  },
  dnb_2016_12_ameriquesud_6: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Aires et périmètres']
  },
  dnb_2016_12_ameriquesud_7: {
    annee: '2016',
    lieu: 'Amérique du sud',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Statistiques', 'Trigonométrie', 'Tableur']
  },
  dnb_2017_05_pondichery_1: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul littéral']
  },
  dnb_2017_05_pondichery_2: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2017_05_pondichery_3: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Programme de calculs', 'Équations']
  },
  dnb_2017_05_pondichery_4: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Fonctions']
  },
  dnb_2017_05_pondichery_5: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Proportionnalité', 'Volumes']
  },
  dnb_2017_05_pondichery_6: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie']
  },
  dnb_2017_05_pondichery_7: {
    annee: '2017',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Aires et périmètres']
  },
  dnb_2017_06_ameriquenord_1: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Fractions', 'Équations']
  },
  dnb_2017_06_ameriquenord_2: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pythagore', 'Aires et périmètres']
  },
  dnb_2017_06_ameriquenord_3: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2017_06_ameriquenord_4: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Proportionnalité']
  },
  dnb_2017_06_ameriquenord_5: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_06_ameriquenord_6: {
    annee: '2017',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Calcul littéral', 'Tableur']
  },
  dnb_2017_06_asie_1: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Fonctions']
  },
  dnb_2017_06_asie_2: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Fractions', 'Proportionnalité']
  },
  dnb_2017_06_asie_3: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie']
  },
  dnb_2017_06_asie_4: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_06_asie_5: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité']
  },
  dnb_2017_06_asie_6: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Calcul littéral']
  },
  dnb_2017_06_asie_7: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2017_06_asie_8: {
    annee: '2017',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Volumes', "Géométrie dans l'espace"]
  },
  dnb_2017_06_etrangers_1: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Fractions', 'Proportionnalité']
  },
  dnb_2017_06_etrangers_2: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Équations', 'Fonctions']
  },
  dnb_2017_06_etrangers_3: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Volumes']
  },
  dnb_2017_06_etrangers_4: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Probabilités']
  },
  dnb_2017_06_etrangers_5: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité']
  },
  dnb_2017_06_etrangers_6: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Algorithmique-programmation']
  },
  dnb_2017_06_etrangers_7: {
    annee: '2017',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Proportionnalité']
  },
  dnb_2017_06_metropole_1: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_06_metropole_2: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_06_metropole_3: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Fonctions']
  },
  dnb_2017_06_metropole_4: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Trigonométrie', 'Pythagore']
  },
  dnb_2017_06_metropole_5: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fonctions']
  },
  dnb_2017_06_metropole_6: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2017_06_metropole_7: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité']
  },
  dnb_2017_06_polynesie_1: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Pythagore', 'Équations', 'Fractions', 'Tableur']
  },
  dnb_2017_06_polynesie_2: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vitesses']
  },
  dnb_2017_06_polynesie_3: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pythagore']
  },
  dnb_2017_06_polynesie_4: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_06_polynesie_5: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Équations']
  },
  dnb_2017_06_polynesie_6: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Durées', 'Arithmétique']
  },
  dnb_2017_09_metropole_1: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_09_metropole_2: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2017_09_metropole_3: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_09_metropole_4: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Proportionnalité', 'Pythagore']
  },
  dnb_2017_09_metropole_5: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Fractions', 'Programme de calculs']
  },
  dnb_2017_09_metropole_6: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Proportionnalité']
  },
  dnb_2017_09_metropole_7: {
    annee: '2017',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Fonctions']
  },
  dnb_2017_09_polynesie_1: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Statistiques', 'Calcul numérique']
  },
  dnb_2017_09_polynesie_2: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Durées', 'Vitesses', 'Fonctions']
  },
  dnb_2017_09_polynesie_3: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Durées']
  },
  dnb_2017_09_polynesie_4: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Géométrie plane']
  },
  dnb_2017_09_polynesie_5: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Durées']
  },
  dnb_2017_09_polynesie_6: {
    annee: '2017',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Programme de calculs', 'Équations']
  },
  dnb_2017_11_ameriquesud_1: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_11_ameriquesud_2: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Géométrie plane']
  },
  dnb_2017_11_ameriquesud_3: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Équations']
  },
  dnb_2017_11_ameriquesud_4: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Fractions', 'Calcul numérique', 'Pythagore', 'Vrai-faux']
  },
  dnb_2017_11_ameriquesud_5: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Fonctions', 'Équations']
  },
  dnb_2017_11_ameriquesud_6: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_11_ameriquesud_7: {
    annee: '2017',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Calcul numérique']
  },
  dnb_2017_12_caledonie_1: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Aires et périmètres', 'Calcul littéral', 'Calcul numérique', 'Fractions', 'Thalès']
  },
  dnb_2017_12_caledonie_2: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Fonctions']
  },
  dnb_2017_12_caledonie_3: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Arithmétique']
  },
  dnb_2017_12_caledonie_4: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Fractions']
  },
  dnb_2017_12_caledonie_5: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2017_12_caledonie_6: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Trigonométrie']
  },
  dnb_2017_12_caledonie_7: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_12_caledonie_8: {
    annee: '2017',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Pythagore']
  },
  dnb_2017_12_wallisfutuna_1: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Fractions', 'Volumes', 'Arithmétique', 'Fonctions']
  },
  dnb_2017_12_wallisfutuna_2: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2017_12_wallisfutuna_3: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2017_12_wallisfutuna_4: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2017_12_wallisfutuna_5: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Fractions']
  },
  dnb_2017_12_wallisfutuna_6: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Arithmétique']
  },
  dnb_2017_12_wallisfutuna_7: {
    annee: '2017',
    lieu: 'Wallis et Futuna',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2018_05_pondichery_1: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2018_05_pondichery_2: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Agrandissement-réduction']
  },
  dnb_2018_05_pondichery_3: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Fractions', "Géométrie dans l'espace"]
  },
  dnb_2018_05_pondichery_4: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Équations', 'Calcul littéral', 'Programme de calculs']

  },
  dnb_2018_05_pondichery_5: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Pythagore', 'Probabilités', 'Géométrie plane']
  },
  dnb_2018_05_pondichery_6: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Durées', 'Grandeurs composées']
  },
  dnb_2018_05_pondichery_7: {
    annee: '2018',
    lieu: 'Pondichéry',
    mois: 'Mai',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Trigonométrie', 'Agrandissement-réduction']
  },
  dnb_2018_06_ameriquenord_1: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Proportionnalité']
  },
  dnb_2018_06_ameriquenord_2: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Thalès', 'Pythagore']
  },
  dnb_2018_06_ameriquenord_3: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2018_06_ameriquenord_4: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2018_06_ameriquenord_5: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Transformations']

  },
  dnb_2018_06_ameriquenord_6: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Volumes', "Prise d'initiatives"]
  },
  dnb_2018_06_ameriquenord_7: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Fonctions']
  },
  dnb_2018_06_ameriquenord_8: {
    annee: '2018',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Durées']
  },
  dnb_2018_06_asie_1: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2018_06_asie_2: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Agrandissement-réduction']
  },
  dnb_2018_06_asie_3: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Probabilités', 'Équations', 'Calcul littéral', 'Fractions']
  },
  dnb_2018_06_asie_4: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Aires et périmètres']
  },
  dnb_2018_06_asie_5: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Calcul littéral', 'Équations']
  },
  dnb_2018_06_asie_6: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Agrandissement-réduction']
  },
  dnb_2018_06_asie_7: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Aires et périmètres']
  },
  dnb_2018_06_asie_8: {
    annee: '2018',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Proportionnalité']
  },
  dnb_2018_06_etrangers_1: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Fractions', 'Calcul littéral', 'Programme de calculs']
  },
  dnb_2018_06_etrangers_2: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Calcul littéral', 'Lecture graphique']
  },
  dnb_2018_06_etrangers_3: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2018_06_etrangers_4: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Volumes']
  },
  dnb_2018_06_etrangers_5: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Fonctions', 'Équations']
  },
  dnb_2018_06_etrangers_6: {
    annee: '2018',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Algorithmique-programmation']
  },
  dnb_2018_06_metropole_1: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', "Géométrie dans l'espace", 'Volumes']
  },
  dnb_2018_06_metropole_2: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2018_06_metropole_3: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2018_06_metropole_4: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pythagore', 'Trigonométrie', 'Triangles semblables']
  },
  dnb_2018_06_metropole_5: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Programme de calculs']
  },
  dnb_2018_06_metropole_6: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Aires et périmètres', 'Algorithmique-programmation']
  },
  dnb_2018_06_metropole_7: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Proportionnalité', 'Équations']
  },
  dnb_2018_07_polynesie_1: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Puissances', 'Pourcentages', 'Probabilités', 'Arithmétique', 'Équations', 'Vrai-faux']

  },
  dnb_2018_07_polynesie_2: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Pourcentages']
  },
  dnb_2018_07_polynesie_3: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2018_07_polynesie_4: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2018_07_polynesie_5: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Proportionnalité']
  },
  dnb_2018_07_polynesie_6: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Équations']
  },
  dnb_2018_09_metropole_1: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Statistiques', 'Pourcentages', 'Tableur']
  },
  dnb_2018_09_metropole_2: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2018_09_metropole_3: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Fonctions']
  },
  dnb_2018_09_metropole_4: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Volumes', 'Pourcentages']
  },
  dnb_2018_09_metropole_5: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2018_09_metropole_6: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral']
  },
  dnb_2018_09_metropole_7: {
    annee: '2018',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Durées']
  },
  dnb_2018_09_polynesie_1: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Puissances', 'Pourcentages', 'Probabilités', 'Arithmétique', 'Équations', 'Vrai-faux']
  },
  dnb_2018_09_polynesie_2: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2018_09_polynesie_3: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions']
  },
  dnb_2018_09_polynesie_4: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Grandeurs composées']
  },
  dnb_2018_09_polynesie_5: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Proportionnalité']
  },
  dnb_2018_09_polynesie_6: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Équations']
  },
  dnb_2018_09_polynesie_7: {
    annee: '2018',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Thalès', 'Pythagore']
  },
  dnb_2018_11_ameriquesud_1: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Trigonométrie', 'Transformations']
  },
  dnb_2018_11_ameriquesud_2: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Pourcentages']
  },
  dnb_2018_11_ameriquesud_3: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Équations', 'Tableur', 'Calcul littéral']
  },
  dnb_2018_11_ameriquesud_4: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', "Prise d'initiatives"]
  },
  dnb_2018_11_ameriquesud_5: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2018_11_ameriquesud_6: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2018_11_ameriquesud_7: {
    annee: '2018',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Grandeurs composées']
  },
  dnb_2018_12_caledonie_1: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul littéral', 'Trigonométrie', 'Arithmétique', 'Thalès']
  },
  dnb_2018_12_caledonie_2: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2018_12_caledonie_3: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2018_12_caledonie_4: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Fonctions', 'Vitesses']
  },
  dnb_2018_12_caledonie_5: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore']
  },
  dnb_2018_12_caledonie_6: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Statistiques', 'Proportionnalité']
  },
  dnb_2018_12_caledonie_7: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Grandeurs composées']
  },
  dnb_2018_12_caledonie_8: {
    annee: '2018',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2019_03_caledonie_1: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Arithmétique', 'Thalès', 'Calcul littéral']
  },
  dnb_2019_03_caledonie_2: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Grandeurs composées', 'Proportionnalité']
  },
  dnb_2019_03_caledonie_3: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Équations']
  },
  dnb_2019_03_caledonie_4: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Vitesses', 'Pourcentages']
  },
  dnb_2019_03_caledonie_5: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Probabilités']
  },
  dnb_2019_03_caledonie_6: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2019_03_caledonie_7: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Pythagore', 'Agrandissement-réduction']
  },
  dnb_2019_03_caledonie_8: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées']
  },
  dnb_2019_06_ameriquenord_1: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Thalès']
  },
  dnb_2019_06_ameriquenord_2: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Fractions', 'Fonctions', 'Probabilités', 'Calcul littéral']

  },
  dnb_2019_06_ameriquenord_3: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Calcul numérique', 'Proportionnalité']
  },
  dnb_2019_06_ameriquenord_4: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2019_06_ameriquenord_5: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Transformations']
  },
  dnb_2019_06_ameriquenord_6: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Calcul numérique']
  },
  dnb_2019_06_ameriquenord_7: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Grandeurs composées', 'Volumes']
  },
  dnb_2019_06_ameriquenord_8: {
    annee: '2019',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2019_06_metropole_1: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Calcul numérique']
  },
  dnb_2019_06_metropole_2: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Transformations']
  },
  dnb_2019_06_metropole_3: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Statistiques', 'Tableur']
  },
  dnb_2019_06_metropole_4: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Thalès']
  },
  dnb_2019_06_metropole_5: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Calcul numérique']
  },
  dnb_2019_06_metropole_6: {
    annee: '2019',
    lieu: 'Antilles - Guyane',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Fonctions', 'Volumes']
  },
  dnb_2019_06_asie_1: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral']

  },
  dnb_2019_06_asie_2: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Fractions']
  },
  dnb_2019_06_asie_3: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2019_06_asie_4: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Grandeurs composées']
  },
  dnb_2019_06_asie_5: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pythagore']
  },
  dnb_2019_06_asie_6: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Calcul numérique']
  },
  dnb_2019_06_asie_7: {
    annee: '2019',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur']
  },
  dnb_2019_06_etrangers_1: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Arithmétique', 'Pourcentages', 'Trigonométrie', 'Statistiques', 'Transformations']
  },
  dnb_2019_06_etrangers_2: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations', 'Tableur']
  },
  dnb_2019_06_etrangers_3: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations', 'Algorithmique-programmation']
  },
  dnb_2019_06_etrangers_4: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2019_06_etrangers_5: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Pythagore']
  },
  dnb_2019_06_etrangers_6: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Lecture graphique', 'Vitesses']
  },
  dnb_2019_06_etrangers_7: {
    annee: '2019',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Calcul numérique', 'Volumes']
  },
  dnb_2019_06_grece_1: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2019_06_grece_2: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Géométrie plane', 'Agrandissement-réduction', 'Triangles semblables']
  },
  dnb_2019_06_grece_3: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Grandeurs composées']
  },
  dnb_2019_06_grece_4: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Transformations']
  },
  dnb_2019_06_grece_5: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Fonctions']
  },
  dnb_2019_06_grece_6: {
    annee: '2019',
    lieu: 'Grèce',
    mois: 'Juin',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Agrandissement-réduction', 'Volumes', 'Pourcentages', "Prise d'initiatives"]
  },
  dnb_2019_07_metropole_1: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2019_07_metropole_2: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Proportionnalité', 'Agrandissement-réduction']
  },
  dnb_2019_07_metropole_3: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Grandeurs composées', 'Statistiques']
  },
  dnb_2019_07_metropole_4: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Probabilités']
  },
  dnb_2019_07_metropole_5: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Transformations']
  },
  dnb_2019_07_metropole_6: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Fonctions', 'Équations', 'Calcul littéral']
  },
  dnb_2019_07_polynesie_1: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Arithmétique', 'Thalès']
  },
  dnb_2019_07_polynesie_2: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Algorithmique-programmation', 'Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2019_07_polynesie_3: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2019_07_polynesie_4: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Agrandissement-réduction', 'Volumes']
  },
  dnb_2019_07_polynesie_5: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore']
  },
  dnb_2019_07_polynesie_6: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Statistiques']
  },
  dnb_2019_07_polynesie_7: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Juillet',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Lecture graphique', 'Grandeurs composées', "Prise d'initiatives"]
  },
  dnb_2019_09_metropole_1: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Vitesses']
  },
  dnb_2019_09_metropole_2: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Puissances']
  },
  dnb_2019_09_metropole_3: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Lecture graphique', 'Fonctions', 'Pourcentages']
  },
  dnb_2019_09_metropole_4: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Volumes']
  },
  dnb_2019_09_metropole_5: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Tableur', 'Calcul littéral', 'Équations']
  },
  dnb_2019_09_metropole_6: {
    annee: '2019',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Algorithmique-programmation']
  },
  dnb_2019_09_polynesie_1: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Vitesses', 'Arithmétique', 'Fonctions', 'Agrandissement-réduction']
  },
  dnb_2019_09_polynesie_2: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Grandeurs composées']
  },
  dnb_2019_09_polynesie_3: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Calcul numérique']
  },
  dnb_2019_09_polynesie_4: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Agrandissement-réduction', 'Thalès', 'Trigonométrie']
  },
  dnb_2019_09_polynesie_5: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Grandeurs composées', 'Volumes']
  },
  dnb_2019_09_polynesie_6: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Lecture graphique', 'Pourcentages', 'Grandeurs composées']
  },
  dnb_2019_09_polynesie_7: {
    annee: '2019',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2019_11_ameriquesud_1: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Statistiques', 'Arithmétique', 'Transformations', 'Agrandissement-réduction']
  },
  dnb_2019_11_ameriquesud_2: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Pourcentages']
  },
  dnb_2019_11_ameriquesud_3: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Équations']
  },
  dnb_2019_11_ameriquesud_4: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie', 'Volumes']
  },
  dnb_2019_11_ameriquesud_5: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', "Prise d'initiatives"]
  },
  dnb_2019_11_ameriquesud_6: {
    annee: '2019',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2019_12_caledonie_1: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Grandeurs composées', 'Durées', 'Puissances', 'Calcul littéral']
  },
  dnb_2019_12_caledonie_2: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Pourcentages']
  },
  dnb_2019_12_caledonie_3: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Agrandissement-réduction']
  },
  dnb_2019_12_caledonie_4: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès']
  },
  dnb_2019_12_caledonie_5: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Lecture graphique']
  },
  dnb_2019_12_caledonie_6: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Calcul numérique', 'Fonctions', 'Équations', 'Lecture graphique']
  },
  dnb_2019_12_caledonie_7: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Grandeurs composées', 'Proportionnalité', "Prise d'initiatives"]
  },
  dnb_2019_12_caledonie_8: {
    annee: '2019',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2020_09_antillesguyanne_1: {
    annee: '2020',
    lieu: 'Antilles - Guyane',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie']
  },
  dnb_2020_09_antillesguyanne_2: {
    annee: '2020',
    lieu: 'Antilles - Guyane',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Agrandissement-réduction', 'Calcul littéral', 'Puissances', 'Fractions']
  },
  dnb_2020_09_antillesguyanne_3: {
    annee: '2020',
    lieu: 'Antilles - Guyane',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Arithmétique']
  },
  dnb_2020_09_antillesguyanne_4: {
    annee: '2020',
    lieu: 'Antilles - Guyane',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages', 'Tableur']
  },
  dnb_2020_09_antillesguyanne_5: {
    annee: '2020',
    lieu: 'Antilles - Guyane',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique', 'Algorithmique-programmation']
  },
  dnb_2020_09_metropole_1: {
    annee: '2020',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Statistiques', 'Probabilités', 'Arithmétique', 'Volumes', 'Transformations']
  },
  dnb_2020_09_metropole_2: {
    annee: '2020',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2020_09_metropole_3: {
    annee: '2020',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Pourcentages', 'Trigonométrie', "Recherche d'informations"]
  },
  dnb_2020_09_metropole_4: {
    annee: '2020',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Fonctions']
  },
  dnb_2020_09_metropole_5: {
    annee: '2020',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Algorithmique-programmation']
  },
  dnb_2020_09_polynesie_1: {
    annee: '2020',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Programme de calculs', 'Thalès', 'Statistiques', 'Pourcentages', 'Arithmétique']
  },
  dnb_2020_09_polynesie_2: {
    annee: '2020',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2020_09_polynesie_3: {
    annee: '2020',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Pythagore', 'Aires et périmètres', 'Agrandissement-réduction']
  },
  dnb_2020_09_polynesie_4: {
    annee: '2020',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2020_09_polynesie_5: {
    annee: '2020',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Équations']
  },
  dnb_2020_12_caledonie_1: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Calcul numérique', 'Puissances', 'Statistiques', 'Probabilités', "Géométrie dans l'espace"]
  },
  dnb_2020_12_caledonie_2: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Tableur']
  },
  dnb_2020_12_caledonie_3: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2020_12_caledonie_4: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Pythagore', 'Trigonométrie', 'Proportionnalité', 'Durées', 'Vitesses']
  },
  dnb_2020_12_caledonie_5: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pythagore']
  },
  dnb_2020_12_caledonie_6: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2020_12_caledonie_7: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Calcul littéral', 'Fonctions']
  },
  dnb_2020_12_caledonie_8: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2021_06_ameriquenord_1: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Calcul littéral', 'Arithmétique', 'Probabilités', 'Trigonométrie', 'Pythagore']
  },
  dnb_2021_06_ameriquenord_2: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Lecture graphique', 'Vitesses']
  },
  dnb_2021_06_ameriquenord_3: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Transformations']
  },
  dnb_2021_06_ameriquenord_4: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives"]
  },
  dnb_2021_06_ameriquenord_5: {
    annee: '2021',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Prise d'initiatives", 'Pourcentages', 'Proportionnalité', 'Aires et périmètres']
  },
  dnb_2021_06_etrangers_1: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Transformations', 'Fractions', 'Calcul numérique', 'Volumes', 'Trigonométrie', 'Aires et périmètres']
  },
  dnb_2021_06_etrangers_2: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2021_06_etrangers_3: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Programme de calculs', 'Équations']
  },
  dnb_2021_06_etrangers_4: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Grandeurs composées', 'Trigonométrie']
  },
  dnb_2021_06_etrangers_5: {
    annee: '2021',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Lecture graphique', 'Équations']
  },
  dnb_2021_06_metropole_1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur']
  },
  dnb_2021_06_metropole_2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Thalès']
  },
  dnb_2021_06_metropole_3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Transformations', 'Agrandissement-réduction', 'QCM']
  },
  dnb_2021_06_metropole_4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Algorithmique-programmation', 'Calcul littéral']
  },
  dnb_2021_06_metropole_5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Volumes', 'Pythagore']
  },
  dnb_2021_06_asie_1: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Fonctions', 'Tableur', 'Équations', 'Puissances']
  },
  dnb_2021_06_asie_2: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    pngCorApmep: 'static/dnb/2021/tex/png/dnb_2021_06_asie_2_cor.png',
    typeExercice: 'dnb',
    urlcorApmep: 'static/dnb/2021/tex/dnb_2021_06_asie_2_cor.tex',
    tags: ['Pythagore', 'Transformations', 'Agrandissement-réduction', 'Trigonométrie']
  },
  dnb_2021_06_asie_3: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Probabilités']
  },
  dnb_2021_06_asie_4: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Vitesses', 'Statistiques', 'Grandeurs composées', 'Vrai-faux']
  },
  dnb_2021_06_asie_5: {
    annee: '2021',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ["Géométrie dans l'espace", 'Volumes', "Prise d'initiatives"]
  },
  dnb_2021_09_metropole_1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Probabilités', 'Transformations', 'Arithmétique', 'Puissances', 'QCM']
  },
  dnb_2021_09_metropole_2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2021_09_metropole_3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Tableur']
  },
  dnb_2021_09_metropole_4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Calcul littéral', 'Géométrie plane']
  },
  dnb_2021_09_metropole_5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pourcentages', 'Thalès', 'Pythagore']
  },
  dnb_2015_03_caledonie_1: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Équations', 'Arithmétique', 'Calcul numérique']

  },
  dnb_2015_03_caledonie_2: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Géométrie plane']
  },
  dnb_2015_03_caledonie_3: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2015_03_caledonie_4: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Système d\'équations', 'Hors programme']
  },
  dnb_2015_03_caledonie_5: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2015_03_caledonie_6: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Aires et périmètres', 'Trigonométrie']
  },
  dnb_2015_03_caledonie_7: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Fonctions', 'Lecture graphique']
  },
  dnb_2015_03_caledonie_8: {
    annee: '2015',
    lieu: 'Nouvelle Calédonie',
    mois: 'Mars',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Pythagore', 'Géométrie plane']
  },
  dnb_2015_06_ameriquenord_7: {
    annee: '2015',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Pythagore', 'Agrandissement-réduction', 'Géométrie dans l\'espace']
  },
  dnb_2021_11_ameriquesud_1: {
    annee: '2021',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Arithmétique', 'Calcul littéral', 'Fonctions', 'Pythagore', 'Thalès']
  },
  dnb_2021_11_ameriquesud_2: {
    annee: '2021',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Lecture graphique', 'Vitesses']
  },
  dnb_2021_11_ameriquesud_3: {
    annee: '2021',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Proportionnalité', 'Ratio', 'Probabilités']
  },
  dnb_2021_11_ameriquesud_4: {
    annee: '2021',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2021_11_ameriquesud_5: {
    annee: '2021',
    lieu: 'Amérique du sud',
    mois: 'Novembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Recherche d\'informations', 'Volumes', 'Pourcentages']
  },
  dnb_2022_06_ameriquenord_1: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie', 'Transformations', 'Agrandissement-réduction']
  },
  dnb_2022_06_ameriquenord_2: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Probabilités', 'Ratio', 'Fonctions', 'Arithmétique', 'Volumes']
  },
  dnb_2022_06_ameriquenord_3: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2022_06_ameriquenord_4: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Probabilités']
  },
  dnb_2022_06_ameriquenord_5: {
    annee: '2022',
    lieu: 'Amérique du Nord',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral']
  },
  dnb_2022_06_asie_1: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Fonctions', 'Volumes']
  },
  dnb_2022_06_asie_2: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Pythagore', 'Transformations', 'Agrandissement-réduction']
  },
  dnb_2022_06_asie_3: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Pourcentages']
  },
  dnb_2022_06_asie_4: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul numérique', 'Algorithmique-programmation', 'Pourcentages']
  },
  dnb_2022_06_asie_5: {
    annee: '2022',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Coordonnées terrestres', 'Aires et périmètres', 'Vitesses']
  },
  dnb_2022_06_etrangers_1: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Fonctions', 'Tableur', 'Calcul littéral', 'Pythagore']
  },
  dnb_2022_06_etrangers_2: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages', 'Durées', 'Vitesses']
  },
  dnb_2022_06_etrangers_3: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Thalès', 'Pythagore']
  },
  dnb_2022_06_etrangers_4: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2022_06_etrangers_5: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Volumes']
  },
  dnb_2022_06_polynesie_1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Calcul numérique', 'Thalès', 'Arithmétique', 'Ratio']
  },
  dnb_2022_06_polynesie_2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Lecture graphique']
  },
  dnb_2022_06_polynesie_3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Probabilités', 'Volumes', 'Équations']
  },
  dnb_2022_06_polynesie_4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral']
  },
  dnb_2022_06_polynesie_5: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Prise d\'initiatives', 'Pythagore', 'Trigonométrie']
  },
  dnb_2022_06_metropole_mathalea_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Pythagore', 'Vitesses']
  },
  dnb_2022_06_metropole_mathalea_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Transformations', 'Fonctions', 'Statistiques', 'Agrandissement-réduction']
  },
  dnb_2022_06_metropole_mathalea_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Probabilités']
  },
  dnb_2022_06_metropole_mathalea_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Algorithmique-programmation', 'Équations']
  },
  dnb_2022_06_metropole_mathalea_5: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Pourcentages', 'Proportionnalité']
  },
  dnb_2022_09_metropole_1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Puissances', 'Arithmétique', 'Calcul littéral', 'Équations', 'Probabilités']
  },
  dnb_2022_09_metropole_2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Équations', 'Lecture graphique']
  },
  dnb_2022_09_metropole_3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Transformations']
  },
  dnb_2022_09_metropole_4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Statistiques', 'Vitesses', 'Grandeurs composées']
  },
  dnb_2022_09_metropole_5: {
    annee: '2022',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Thalès', 'Volumes']
  },
  dnb_2022_09_polynesie_1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fractions', 'Arithmétique', 'Calcul littéral', 'Volumes', 'Pourcentages']
  },
  dnb_2022_09_polynesie_2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Vitesses', 'Thalès']
  },
  dnb_2022_09_polynesie_3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Transformations']
  },
  dnb_2022_09_polynesie_4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Programme de calculs', 'Équations', 'Lecture graphique']
  },
  dnb_2022_09_polynesie_5: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Statistiques']
  },
  dnb_2023_05_ameriquenord_1: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Probabilités', 'Calcul littéral', 'Volumes', 'Agrandissement-réduction']
  },
  dnb_2023_05_ameriquenord_2: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie', 'Aires et périmètres', 'Proportionnalité']
  },
  dnb_2023_05_ameriquenord_3: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2023_05_ameriquenord_4: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Géométrie plane']
  },
  dnb_2023_05_ameriquenord_5: {
    annee: '2023',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Vitesses', 'Proportionnalité']
  },
  dnb_2023_06_asie_1: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Proportionnalité', 'Aires et périmètres']
  },
  dnb_2023_06_asie_2: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['QCM', 'Probabilités', 'Pourcentages', 'Transformations', 'Fonctions', 'Puissances', 'Trigonométrie']
  },
  dnb_2023_06_asie_3: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations', 'Tableur']
  },
  dnb_2023_06_asie_4: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2023_06_asie_5: {
    annee: '2023',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Recherche d\'informations', 'Statistiques', 'Pourcentages', 'Volumes']
  },
  dnb_2023_06_etrangers_1: {
    annee: '2023',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Algorithmique-programmation', 'Calcul numérique', 'Puissances', 'Statistiques']
  },
  dnb_2023_06_etrangers_2: {
    annee: '2023',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Thalès', 'Volumes', 'Recherche d\'informations']
  },
  dnb_2023_06_etrangers_3: {
    annee: '2023',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Tableur', 'Calcul littéral', 'Équations']
  },
  dnb_2023_06_etrangers_4: {
    annee: '2023',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2023_06_etrangers_5: {
    annee: '2023',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Lecture graphique', 'Proportionnalité']
  },
  dnb_2023_06_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Statistiques']
  },
  dnb_2023_06_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Pythagore', 'Thalès']
  },
  dnb_2023_06_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['QCM', 'Pourcentages', 'Probabilités', 'Transformations', 'Volumes']
  },
  dnb_2023_06_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Recherche d\'informations', 'Trigonométrie', 'Algorithmique-programmation']
  },
  dnb_2023_06_metropole_5: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Fonctions', 'Équations']
  },
  dnb_2023_06_polynesie_1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Fonctions', 'Tableur', 'Calcul littéral']
  },
  dnb_2023_06_polynesie_2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Recherche d\'informations', 'Pythagore', 'Pourcentages', 'Trigonométrie', 'Thalès']
  },
  dnb_2023_06_polynesie_3: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2023_06_polynesie_4: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Calcul littéral', 'Programme de calculs', 'Équations']
  },
  dnb_2023_06_polynesie_5: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Vitesses', 'Arithmétique']
  },
  dnb_2023_09_metropole_1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Arithmétique', 'Agrandissement-réduction', 'Équations', 'Probabilités', 'Transformations']
  },
  dnb_2023_09_metropole_2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2023_09_metropole_3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Équations', 'Statistiques']
  },
  dnb_2023_09_metropole_4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Recherche d\'informations', 'Pourcentages', 'Proportionnalité']
  },
  dnb_2023_09_metropole_5: {
    annee: '2023',
    lieu: 'Métropole',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie', 'Aires et périmètres']
  },
  dnb_2023_09_polynesie_1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Pourcentages', 'Thalès', 'Probabilités']
  },
  dnb_2023_09_polynesie_2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Fonctions', 'Tableur']
  },
  dnb_2023_09_polynesie_3: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2023_09_polynesie_4: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Volumes']
  },
  dnb_2023_09_polynesie_5: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Coordonnées terrestres', 'Durées', 'Statistiques', 'Vitesses']
  },

  dnb_2023_10_amsud_1: {
    annee: '2023',
    lieu: 'amsud',
    mois: 'Octobre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Géométrie plane', 'Agrandissement-réduction', 'Aires et périmètres']
  },
  dnb_2023_10_amsud_2: {
    annee: '2023',
    lieu: 'amsud',
    mois: 'Octobre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Calcul littéral', 'Fonctions', 'Tableur', 'Lecture graphique']
  },
  dnb_2023_10_amsud_3: {
    annee: '2023',
    lieu: 'amsud',
    mois: 'Octobre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Proportionnalité', 'Arithmétique', 'Calcul littéral']
  },
  dnb_2023_10_amsud_4: {
    annee: '2023',
    lieu: 'amsud',
    mois: 'Octobre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Grandeurs composées', 'Proportionnalité']
  },
  dnb_2023_10_amsud_5: {
    annee: '2023',
    lieu: 'amsud',
    mois: 'Octobre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Algorithmique-programmation']
  },
  dnb_2023_12_caledonie_1: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['QCM', 'Probabilités', 'Puissances', 'Calcul numérique', 'Transformations', 'Statistiques']
  },
  dnb_2023_12_caledonie_2: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique']
  },
  dnb_2023_12_caledonie_3: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Aires et périmètres', 'Pythagore', 'Volumes', 'Grandeurs composées']
  },
  dnb_2023_12_caledonie_4: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Thalès', 'Vitesses', 'Durées']
  },
  dnb_2023_12_caledonie_5: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Calcul littéral', 'Équations']
  },
  dnb_2023_12_caledonie_6: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Algorithmique-programmation']
  },

  dnb_2021_12_caledonie_1: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: [
      'Pourcentages', 'Fractions', 'Calcul littéral', 'Équations', 'Calcul numérique', 'Volumes', 'Trigonométrie', 'Géométrie plane'
    ]
  },
  dnb_2021_12_caledonie_2: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: [
      'Statistiques', 'Pourcentages'
    ]
  },
  dnb_2021_12_caledonie_3: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: [
      'Géométrie plane', 'Pythagore'
    ]
  },
  dnb_2021_12_caledonie_4: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: [
      'Arithmétique', 'Pourcentages'
    ]
  },
  dnb_2021_12_caledonie_5: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: [
      'Probabilités', 'Fractions'
    ]
  },
  dnb_2021_12_caledonie_6: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: [
      'Proportionnalité', 'Lecture graphique', 'Tableur'
    ]
  },
  dnb_2021_12_caledonie_7: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: [
      "Recherche d'informations", 'Aires et périmètres'
    ]
  },
  dnb_2021_12_caledonie_8: {
    annee: '2021',
    lieu: 'Nouvelle Calédonie',
    mois: 'Décembre',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: [
      'Géométrie plane', 'Thalès', 'Algorithmique-programmation'
    ]
  },

  dnb_2021_06_polynesie_1: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Transformations', 'Calcul littéral', 'Équations', 'Arithmétique', 'Fractions', 'Coordonnées terrestres']
  },
  dnb_2021_06_polynesie_2: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Fractions', 'Équations']
  },
  dnb_2021_06_polynesie_3: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres', 'Trigonométrie', 'Géométrie plane']
  },
  dnb_2021_06_polynesie_4: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2021_06_polynesie_5: {
    annee: '2021',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Lecture graphique', 'Proportionnalité', 'Fonctions', 'Équations']
  },

  dnb_2024_05_ameriquenord_1: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Vrai-faux', 'Statistiques', 'Vitesses', 'Probabilités', 'Agrandissement-réduction']
  },
  dnb_2024_05_ameriquenord_2: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2024_05_ameriquenord_3: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Lecture graphique', 'Proportionnalité']
  },
  dnb_2024_05_ameriquenord_4: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Pourcentages', 'Proportionnalité', 'Aires et périmètres']
  },
  dnb_2024_05_ameriquenord_5: {
    annee: '2024',
    lieu: 'Amérique du Nord',
    mois: 'Mai',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Thalès', 'Programme de calculs']
  },

  dnb_2024_06_asie_1: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Aires et périmètres', 'Calcul littéral', 'Ratio', 'Statistiques']
  },
  dnb_2024_06_asie_2: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ["Géométrie dans l'espace", 'Thalès', 'Probabilités']
  },
  dnb_2024_06_asie_3: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres', 'Trigonométrie', 'Géométrie plane']
  },
  dnb_2024_06_asie_4: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Fonctions', "Recherche d'informations", 'Équations', 'Lecture graphique']
  },
  dnb_2024_06_asie_5: {
    annee: '2024',
    lieu: 'Asie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation']
  },
  dnb_2024_06_etrangers_1: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Puissances', 'Vitesses', 'Probabilités', 'Fractions', 'Statistiques']
  },
  dnb_2024_06_etrangers_2: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Arithmétique', "Prise d'initiatives"]
  },
  dnb_2024_06_etrangers_3: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Algorithmique-programmation', 'Fonctions', 'Calcul littéral', 'Équations']
  },
  dnb_2024_06_etrangers_4: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Trigonométrie', 'Thalès', 'Transformations']
  },
  dnb_2024_06_etrangers_5: {
    annee: '2024',
    lieu: 'Centres étrangers',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Aires et périmètres', 'Proportionnalité', 'Volumes', 'Agrandissement-réduction']
  },

  dnb_2020_02_caledonie_1: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Thalès', 'Géométrie plane']
  },
  dnb_2020_02_caledonie_2: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Programme de calculs', 'Calcul littéral', 'Équations']
  },
  dnb_2020_02_caledonie_3: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Lecture graphique', 'Statistiques']
  },
  dnb_2020_02_caledonie_4: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Pythagore', 'Grandeurs composées']
  },
  dnb_2020_02_caledonie_5: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Proportionnalité', 'Pourcentages']
  },
  dnb_2020_02_caledonie_6: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '6',
    typeExercice: 'dnb',
    tags: ['Tableur', 'Statistiques', 'Probabilités']
  },
  dnb_2020_02_caledonie_7: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '7',
    typeExercice: 'dnb',
    tags: ['Géométrie plane', 'Trigonométrie', "Prise d'initiatives"]
  },
  dnb_2020_02_caledonie_8: {
    annee: '2020',
    lieu: 'Nouvelle Calédonie',
    mois: 'Février',
    numeroInitial: '8',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', "Recherche d'informations"]
  },

  dnb_2024_06_polynesie_1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Fonctions', 'Transformations', 'Ratio', 'Arithmétique', 'QCM']
  },
  dnb_2024_06_polynesie_2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Pourcentages']
  },
  dnb_2024_06_polynesie_3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie', 'Volumes', 'Aires et périmètres']
  },
  dnb_2024_06_polynesie_4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2024_06_polynesie_5: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Juin',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Fonctions', 'Tableur', 'Équations', 'Algorithmique-programmation', 'Programme de calculs', 'Calcul littéral']
  },
  dnb_2024_07_antilles_1: {
    annee: '2024',
    lieu: 'Antilles Martinique',
    mois: 'Juillet',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités', 'Arithmétique']
  },
  dnb_2024_07_antilles_2: {
    annee: '2024',
    lieu: 'Antilles Martinique',
    mois: 'Juillet',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Puissances', 'Statistiques', 'Transformations', 'Fonctions']
  },
  dnb_2024_07_antilles_3: {
    annee: '2024',
    lieu: 'Antilles Martinique',
    mois: 'Juillet',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Trigonométrie']
  },
  dnb_2024_07_antilles_4: {
    annee: '2024',
    lieu: 'Antilles Martinique',
    mois: 'Juillet',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Programme de calculs', 'Calcul littéral', 'Algorithmique-programmation', 'Équations']
  },
  dnb_2024_07_antilles_5: {
    annee: '2024',
    lieu: 'Antilles Martinique',
    mois: 'Juillet',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Aires et périmètres', 'Volumes', 'Grandeurs composées']
  },
  dnb_2024_09_polynesie_1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Puissances', 'Grandeurs composées', 'Équations', 'Fonctions', 'Thalès']
  },
  dnb_2024_09_polynesie_2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Statistiques', 'Tableur', 'Probabilités', 'Pourcentages']
  },
  dnb_2024_09_polynesie_3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Trigonométrie', 'Pythagore', 'Triangles égaux', 'Agrandissement-réduction', 'Aires et périmètres']
  },
  dnb_2024_09_polynesie_4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Algorithmique-programmation', 'Transformations']
  },
  dnb_2024_09_polynesie_5: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: 'Septembre',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Volumes', 'Fonctions', 'Vitesses', 'Grandeurs composées']
  },
  dnb_2024_07_metropole_1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '1',
    typeExercice: 'dnb',
    tags: ['Probabilités']
  },
  dnb_2024_07_metropole_2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '2',
    typeExercice: 'dnb',
    tags: ['Calcul littéral', 'Algorithmique-programmation', 'Programme de calculs', 'Équations']
  },
  dnb_2024_07_metropole_3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '3',
    typeExercice: 'dnb',
    tags: ['Pythagore', 'Thalès', 'Aires et périmètres', 'Pourcentages']
  },
  dnb_2024_07_metropole_4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '4',
    typeExercice: 'dnb',
    tags: ['Puissances', 'Fonctions', 'QCM', 'Transformations', 'Statistiques', 'Trigonométrie']
  },
  dnb_2024_07_metropole_5: {
    annee: '2024',
    lieu: 'Métropole',
    mois: 'Juillet',
    numeroInitial: '5',
    typeExercice: 'dnb',
    tags: ['Arithmétique', 'Volumes', 'Fractions']
  },
}
